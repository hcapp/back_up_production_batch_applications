package com.scansee.batch.common;

import java.util.Calendar;

/**
 * The BatchTime class gives batch process information.
 * @author sowjanya_d
 *
 */
public class BatchTime
{
	/**
	 * Constructor with detail messages.
	 * @param calendar1
	 *          as parameter
	 * @param calendar2
	 *          as parameter
	 */
	public BatchTime(Calendar calendar1,Calendar calendar2)
	{
		final int year1 = calendar1.get(Calendar.YEAR);
		final int month1 = calendar1.get(Calendar.MONTH);
		final int date1 = calendar1.get(Calendar.DATE);
		final int hours1 = calendar1.get(Calendar.HOUR);
		final int minute1 = calendar1.get(Calendar.MINUTE);
		final int seconds1 = calendar1.get(Calendar.SECOND);
		
		final int year2 = calendar2.get(Calendar.YEAR);
		final int month2 = calendar2.get(Calendar.MONTH);
		final int date2 = calendar2.get(Calendar.DATE);
		final int hours2 = calendar2.get(Calendar.HOUR);
		final int minute2 = calendar2.get(Calendar.MINUTE);
		final int seconds2 = calendar2.get(Calendar.SECOND);
		
		calendar1.set(year1, month1, date1, hours1, minute1, seconds1);
		calendar2.set(year2, month2, date2, hours2, minute2, seconds2);
		
		final long milliseconds1 = calendar1.getTimeInMillis();
		final long milliseconds2 = calendar2.getTimeInMillis();
		final long diff = milliseconds2 - milliseconds1;
		final long diffSeconds = diff / 1000;
		final long diffMinutes = diff / (60 * 1000);
		final long diffHours = diff / (60 * 60 * 1000);
		final long diffDays = diff / (24 * 60 * 60 * 1000);
		System.out.println("Diff in days--->" + diffDays + "<---hours--->" + diffHours + "<---minutes--->" + diffMinutes + "<---seconds--->" + diffSeconds+"<---milliseconds--->" + diff);
	}
	
}