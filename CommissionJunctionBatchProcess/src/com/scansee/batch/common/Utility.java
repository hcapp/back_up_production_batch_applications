package com.scansee.batch.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.util.HSSFColor.LAVENDER;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import au.com.bytecode.opencsv.CSVReader;

import com.scansee.batch.common.pojos.BatchProcessStatus;
import com.scansee.batch.common.pojos.CommissionJuncBatchStatus;
import com.scansee.batch.common.pojos.OnlineRetailerInfo;
import com.scansee.batch.exception.CommissionJunctionBatchProcessException;

public class Utility
{

	/**
	 * Get logger instance.
	 */
	private static final Logger LOG = Logger.getLogger(Utility.class);

	/**
	 * THis method is used for fetching commission junction online retailer
	 * list.
	 * 
	 * @param commiJunctFile
	 *            contains online retailer information
	 * @return ArrayList<OnlineRetailerInfo> of containing online retailer list
	 *         details.
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws CommissionJunctionBatchProcessException
	 */
	public static ArrayList<OnlineRetailerInfo> getOnlineRetailerList(File file, int index) throws FileNotFoundException, IOException,
			CommissionJunctionBatchProcessException
	{
		LOG.info("Inside Utility : getOnlineRetailerList ");

		OnlineRetailerInfo onlineRetailerInfoObj = null;
		ArrayList<OnlineRetailerInfo> onlineRetailerInfolst = new ArrayList<OnlineRetailerInfo>();
		BufferedReader br;
		String[] nextLine;
		try
		{

			br = new BufferedReader(new FileReader(file.getPath()));
			CSVReader reader = new CSVReader(br);
			// String csvFile =

			// String csvFile = file.getPath();
			// CSVReader reader = new CSVReader(new FileReader(csvFile), ',');

			// List<String[]> csvEntries = reader.readAll();

			/*
			 * if (csvEntries.size() > 0) { Iterator<String[]> iterator =
			 * csvEntries.iterator(); while (iterator.hasNext()) {
			 */

			while ((nextLine = reader.readNext()) != null)
			{

				onlineRetailerInfoObj = new OnlineRetailerInfo();
				if (nextLine.length == 40)
				{
					if (nextLine[0] != null && !nextLine[0].equals(""))
					{
						onlineRetailerInfoObj.setPrgName(nextLine[0]);
					}
					else
					{
						onlineRetailerInfoObj.setPrgName(null);
					}

					if (nextLine[1] != null && !nextLine[1].equals(""))
					{
						onlineRetailerInfoObj.setPrgUrl(nextLine[1]);
					}
					else
					{
						onlineRetailerInfoObj.setPrgUrl(null);
					}

					if (nextLine[2] != null && !nextLine[2].equals(""))
					{
						onlineRetailerInfoObj.setCatLogName(nextLine[2]);
					}
					else
					{
						onlineRetailerInfoObj.setCatLogName(null);
					}

					if (nextLine[3] != null && !nextLine[3].equals(""))
					{
						onlineRetailerInfoObj.setLastUpdateDate(nextLine[3]);
					}
					else
					{
						onlineRetailerInfoObj.setLastUpdateDate(null);
					}

					if (nextLine[4] != null && !nextLine[4].equals("") && !nextLine[4].equals("NULL"))
					{
						onlineRetailerInfoObj.setRetStoreName(nextLine[4]);
					}
					else
					{
						onlineRetailerInfoObj.setRetStoreName(null);
					}

					if (nextLine[5] != null && !nextLine[5].equals(""))
					{
						onlineRetailerInfoObj.setKeyword(nextLine[5]);
					}
					else
					{
						onlineRetailerInfoObj.setKeyword(null);
					}

					if (nextLine[6] != null && !nextLine[6].equals(""))
					{
						onlineRetailerInfoObj.setDescription(nextLine[6]);
					}
					else
					{
						onlineRetailerInfoObj.setDescription(null);
					}

					if (nextLine[7] != null && !nextLine[7].equals(""))
					{
						onlineRetailerInfoObj.setSku(nextLine[7]);
					}
					else
					{
						onlineRetailerInfoObj.setSku(null);
					}

					if (nextLine[8] != null && !nextLine[8].equals(""))
					{
						onlineRetailerInfoObj.setManufacturer(nextLine[8]);
					}
					else
					{
						onlineRetailerInfoObj.setManufacturer(null);
					}

					if (nextLine[9] != null && !nextLine[9].equals(""))
					{
						onlineRetailerInfoObj.setManufacturerId(nextLine[9]);
					}
					else
					{
						onlineRetailerInfoObj.setManufacturerId(null);
					}

					if (nextLine[10] != null && !nextLine[10].equals("") && !nextLine[10].equals("NULL"))
					{
						onlineRetailerInfoObj.setUpc(nextLine[10]);
					}
					else
					{
						onlineRetailerInfoObj.setUpc(null);
					}

					if (nextLine[11] != null && !nextLine[11].equals("") && !nextLine[11].equals("NULL"))
					{
						onlineRetailerInfoObj.setIsbn(nextLine[11]);
					}
					else
					{
						onlineRetailerInfoObj.setIsbn(null);
					}

					if (nextLine[12] != null && !nextLine[12].equals(""))
					{
						onlineRetailerInfoObj.setCurrency(nextLine[12]);
					}
					else
					{
						onlineRetailerInfoObj.setCurrency(null);
					}

					if (nextLine[13] != null && !nextLine[13].equals(""))
					{

						try
						{
							Double.valueOf(nextLine[13]);
							onlineRetailerInfoObj.setSalePrice(nextLine[13]);
						}
						catch (NumberFormatException e)
						{
							onlineRetailerInfoObj.setSalePrice(null);
						}

					}
					else
					{
						onlineRetailerInfoObj.setSalePrice(null);
					}

					if (nextLine[14] != null && !nextLine[14].equals(""))
					{

						try
						{

							Double.valueOf(nextLine[14]);
							onlineRetailerInfoObj.setPrice(nextLine[14]);
						}
						catch (NumberFormatException exception)
						{
							onlineRetailerInfoObj.setPrice(null);
						}

					}
					else
					{
						onlineRetailerInfoObj.setPrice(null);
					}

					if (nextLine[15] != null && !nextLine[15].equals(""))
					{

						try
						{
							Double.valueOf(nextLine[15]);
							onlineRetailerInfoObj.setRetPrice(nextLine[15]);
						}
						catch (NumberFormatException e)
						{
							onlineRetailerInfoObj.setRetPrice(null);
						}

					}
					else
					{
						onlineRetailerInfoObj.setRetPrice(null);
					}

					if (nextLine[16] != null && !nextLine[16].equals(""))
					{
						onlineRetailerInfoObj.setFrmPrice(nextLine[16]);
					}
					else
					{
						onlineRetailerInfoObj.setFrmPrice(null);
					}

					if (nextLine[17] != null && !nextLine[17].equals(""))
					{
						onlineRetailerInfoObj.setBuyUrl(nextLine[17]);
					}
					else
					{
						onlineRetailerInfoObj.setBuyUrl(null);
					}

					if (nextLine[18] != null && !nextLine[18].equals(""))
					{
						onlineRetailerInfoObj.setImpressionUrl(nextLine[18]);
					}
					else
					{
						onlineRetailerInfoObj.setImpressionUrl(null);
					}

					if (nextLine[19] != null && !nextLine[19].equals(""))
					{
						onlineRetailerInfoObj.setImageUrl(nextLine[19]);
					}
					else
					{
						onlineRetailerInfoObj.setImageUrl(null);
					}

					if (nextLine[20] != null && !nextLine[20].equals(""))
					{
						onlineRetailerInfoObj.setAdvCategory(nextLine[20]);
					}
					else
					{
						onlineRetailerInfoObj.setAdvCategory(null);
					}

					if (nextLine[21] != null && !nextLine[21].equals(""))
					{
						onlineRetailerInfoObj.setThirdPartyId(nextLine[21]);
					}
					else
					{
						onlineRetailerInfoObj.setThirdPartyId(null);
					}

					if (nextLine[22] != null && !nextLine[22].equals(""))
					{
						onlineRetailerInfoObj.setThirdPartyCat(nextLine[22]);
					}
					else
					{
						onlineRetailerInfoObj.setThirdPartyCat(null);
					}

					if (nextLine[23] != null && !nextLine[23].equals(""))
					{
						onlineRetailerInfoObj.setAuthor(nextLine[23]);
					}
					else
					{
						onlineRetailerInfoObj.setAuthor(null);
					}

					if (nextLine[24] != null && !nextLine[24].equals(""))
					{
						onlineRetailerInfoObj.setArtist(nextLine[24]);
					}
					else
					{
						onlineRetailerInfoObj.setArtist(null);
					}

					if (nextLine[25] != null && !nextLine[25].equals(""))
					{
						onlineRetailerInfoObj.setTitle(nextLine[25]);
					}
					else
					{
						onlineRetailerInfoObj.setTitle(null);
					}

					if (nextLine[26] != null && !nextLine[26].equals(""))
					{
						onlineRetailerInfoObj.setPublisher(nextLine[26]);
					}
					else
					{
						onlineRetailerInfoObj.setPublisher(null);
					}

					if (nextLine[27] != null && !nextLine[27].equals(""))
					{
						onlineRetailerInfoObj.setLabel(nextLine[27]);
					}
					else
					{
						onlineRetailerInfoObj.setLabel(null);
					}

					if (nextLine[28] != null && !nextLine[28].equals(""))
					{
						onlineRetailerInfoObj.setFormat(nextLine[28]);
					}
					else
					{
						onlineRetailerInfoObj.setFormat(null);
					}

					if (nextLine[29] != null && !nextLine[29].equals(""))
					{
						onlineRetailerInfoObj.setSpecial(nextLine[29]);
					}
					else
					{
						onlineRetailerInfoObj.setSpecial(null);
					}

					if (nextLine[30] != null && !nextLine[30].equals(""))
					{
						onlineRetailerInfoObj.setGift(nextLine[30]);
					}
					else
					{
						onlineRetailerInfoObj.setGift(null);
					}

					if (nextLine[31] != null && !nextLine[31].equals(""))
					{
						onlineRetailerInfoObj.setPromotionalText(nextLine[31]);
					}
					else
					{
						onlineRetailerInfoObj.setPromotionalText(null);
					}

					if (nextLine[32] != null && !nextLine[32].equals(""))
					{
						SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
						try
						{
							dateFormat.parse(nextLine[32]);
							onlineRetailerInfoObj.setStartDate(nextLine[32]);

						}
						catch (Exception e)
						{
							onlineRetailerInfoObj.setStartDate(null);
						}

					}
					else
					{
						onlineRetailerInfoObj.setStartDate(null);
					}

					if (nextLine[33] != null && !nextLine[33].equals(""))
					{

						SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
						try
						{
							dateFormat.parse(nextLine[33]);
							onlineRetailerInfoObj.setEndDate(nextLine[33]);

						}
						catch (Exception e)
						{
							onlineRetailerInfoObj.setStartDate(null);
						}

					}
					else
					{
						onlineRetailerInfoObj.setEndDate(null);
					}

					if (nextLine[34] != null && !nextLine[34].equals(""))
					{
						onlineRetailerInfoObj.setOffline(nextLine[34]);
					}
					else
					{
						onlineRetailerInfoObj.setOffline(null);
					}

					if (nextLine[35] != null && !nextLine[35].equals(""))
					{
						onlineRetailerInfoObj.setOnline(nextLine[35]);
					}
					else
					{
						onlineRetailerInfoObj.setOnline(null);
					}

					if (nextLine[36] != null && !nextLine[36].equals(""))
					{
						onlineRetailerInfoObj.setInstock(nextLine[36]);
					}
					else
					{
						onlineRetailerInfoObj.setInstock(null);
					}

					if (nextLine[37] != null && !nextLine[37].equals(""))
					{
						onlineRetailerInfoObj.setCondition(nextLine[37]);
					}
					else
					{
						onlineRetailerInfoObj.setCondition(null);
					}

					if (nextLine[38] != null && !nextLine[38].equals(""))
					{
						onlineRetailerInfoObj.setWarranty(nextLine[38]);
					}
					else
					{
						onlineRetailerInfoObj.setWarranty(null);
					}

					if (nextLine[39] != null && !nextLine[39].equals(""))
					{
						onlineRetailerInfoObj.setShippingCost(nextLine[39]);
					}
					else
					{
						onlineRetailerInfoObj.setShippingCost(null);
					}

					/*
					 * if ( nextLine[40].) {
					 * onlineRetailerInfoObj.setCategory(nextLine[40]); } if (
					 * null !=nextLine[41] ) {
					 * onlineRetailerInfoObj.setSubCategory(nextLine[41]); }
					 */

					onlineRetailerInfolst.add(onlineRetailerInfoObj);

				}
			}

			if (index == 0)
			{
				// Remove header instruction names from *.csv file */
				onlineRetailerInfolst.remove(0);
			}

			// }*/
		}
		catch (IOException exception)
		{
			LOG.info("Error Occurred in Utility : getOnlineRetailerList : " + exception);

			LOG.info("File Name:" + file.getName());
			throw new CommissionJunctionBatchProcessException(exception);
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			LOG.info("Error Occurred in Utility : getOnlineRetailerList : " + e);
			LOG.info("File Name:" + file.getName());
			throw new CommissionJunctionBatchProcessException(e);
		}
		catch (Exception exception)
		{
			LOG.info("Error Occurred in Utility : getOnlineRetailerList : " + exception);
			LOG.info("File Name:" + file.getName());
			throw new CommissionJunctionBatchProcessException(exception);
		}
		// System.out.println("file processed");

		return onlineRetailerInfolst;
	}

	public static boolean isEmpty(String value)
	{
		final String methodName = "isEmptyString";
		LOG.info("method name : " + methodName);
		final boolean emptyString;

		if ("".equals(value))
		{
			emptyString = true;
		}
		else
		{
			emptyString = false;
		}
		LOG.info("method end : " + methodName);
		return emptyString;
	}

	@SuppressWarnings("rawtypes")
	public static ArrayList<OnlineRetailerInfo> getCJDataFromXLSXFile(InputStream inputStream) throws CommissionJunctionBatchProcessException
	{
		LOG.info("INSIDE getCJDataFromXLSXFile METHOD");
		XSSFSheet sheet = null;
		ArrayList<OnlineRetailerInfo> onlineRetailerInfolst = new ArrayList<OnlineRetailerInfo>();
		try
		{
			XSSFWorkbook wb_xssf = new XSSFWorkbook(inputStream);

			sheet = wb_xssf.getSheetAt(0);
			Iterator rowItr = sheet.rowIterator();

			while (rowItr.hasNext())
			{
				XSSFRow row = (XSSFRow) rowItr.next();

				OnlineRetailerInfo onlineRetailerInfoObj = new OnlineRetailerInfo();
				onlineRetailerInfoObj.setPrgName(((row.getCell(0)) != null) ? row.getCell(0).toString() : null);
				onlineRetailerInfoObj.setPrgUrl((row.getCell(1) != null) ? row.getCell(1).toString() : null);
				onlineRetailerInfoObj.setCatLogName((row.getCell(2) != null) ? row.getCell(2).toString() : null);
				onlineRetailerInfoObj.setLastUpdateDate((row.getCell(3) != null) ? row.getCell(3).toString() : null);
				onlineRetailerInfoObj.setRetStoreName((row.getCell(4) != null) ? row.getCell(4).toString() : null);
				onlineRetailerInfoObj.setKeyword((row.getCell(5) != null) ? row.getCell(5).toString() : null);
				onlineRetailerInfoObj.setDescription((row.getCell(6) != null) ? row.getCell(6).toString() : null);
				onlineRetailerInfoObj.setSku((row.getCell(7) != null) ? row.getCell(7).toString() : null);
				onlineRetailerInfoObj.setManufacturer((row.getCell(8) != null) ? row.getCell(8).toString() : null);
				onlineRetailerInfoObj.setManufacturerId((row.getCell(9) != null) ? row.getCell(9).toString() : null);
				onlineRetailerInfoObj.setUpc((row.getCell(10) != null) ? row.getCell(10).toString() : null);
				onlineRetailerInfoObj.setIsbn((row.getCell(11) != null) ? row.getCell(11).toString() : null);
				onlineRetailerInfoObj.setCurrency((row.getCell(12) != null) ? row.getCell(12).toString() : null);

				onlineRetailerInfoObj.setSalePrice((row.getCell(13) != null) ? row.getCell(13).toString() : null);
				onlineRetailerInfoObj.setPrice((row.getCell(14) != null) ? row.getCell(14).toString() : null);
				onlineRetailerInfoObj.setRetPrice((row.getCell(15) != null) ? row.getCell(15).toString() : null);
				onlineRetailerInfoObj.setFrmPrice((row.getCell(16) != null) ? row.getCell(16).toString() : null);
				onlineRetailerInfoObj.setBuyUrl((row.getCell(17) != null) ? row.getCell(17).toString() : null);
				onlineRetailerInfoObj.setImpressionUrl((row.getCell(18) != null) ? row.getCell(18).toString() : null);
				onlineRetailerInfoObj.setImageUrl((row.getCell(19) != null) ? row.getCell(19).toString() : null);
				onlineRetailerInfoObj.setAdvCategory((row.getCell(20) != null) ? row.getCell(20).toString() : null);
				onlineRetailerInfoObj.setThirdPartyId((row.getCell(21) != null) ? row.getCell(21).toString() : null);
				onlineRetailerInfoObj.setThirdPartyCat((row.getCell(22) != null) ? row.getCell(22).toString() : null);
				onlineRetailerInfoObj.setArtist((row.getCell(23) != null) ? row.getCell(23).toString() : null);
				onlineRetailerInfoObj.setAuthor((row.getCell(24) != null) ? row.getCell(24).toString() : null);
				onlineRetailerInfoObj.setTitle((row.getCell(25) != null) ? row.getCell(25).toString() : null);
				onlineRetailerInfoObj.setPublisher((row.getCell(26) != null) ? row.getCell(26).toString() : null);
				onlineRetailerInfoObj.setLabel((row.getCell(27) != null) ? row.getCell(27).toString() : null);
				onlineRetailerInfoObj.setFormat((row.getCell(28) != null) ? row.getCell(28).toString() : null);
				onlineRetailerInfoObj.setSpecial((row.getCell(29) != null) ? row.getCell(29).toString() : null);
				onlineRetailerInfoObj.setGift((row.getCell(30) != null) ? row.getCell(30).toString() : null);
				onlineRetailerInfoObj.setPromotionalText((row.getCell(31) != null) ? row.getCell(31).toString() : null);
				if (row.getRowNum() != 0)
				{

					onlineRetailerInfoObj.setStartDate((row.getCell(32) != null) ? formattedDate(row.getCell(32).getDateCellValue().toString())
							: null);
					onlineRetailerInfoObj.setEndDate((row.getCell(33) != null) ? formattedDate(row.getCell(33).getDateCellValue().toString()) : null);

				}
				else
				{

					onlineRetailerInfoObj.setStartDate((row.getCell(32) != null) ? row.getCell(32).toString() : null);
					onlineRetailerInfoObj.setEndDate((row.getCell(33) != null) ? row.getCell(33).toString() : null);

				}
				onlineRetailerInfoObj.setOffline((row.getCell(34) != null) ? row.getCell(34).toString() : null);
				onlineRetailerInfoObj.setOnline((row.getCell(35) != null) ? row.getCell(35).toString() : null);
				onlineRetailerInfoObj.setInstock((row.getCell(36) != null) ? row.getCell(36).toString() : null);
				onlineRetailerInfoObj.setCondition((row.getCell(37) != null) ? row.getCell(37).toString() : null);
				onlineRetailerInfoObj.setWarranty((row.getCell(38) != null) ? row.getCell(38).toString() : null);
				onlineRetailerInfoObj.setShippingCost((row.getCell(39) != null) ? row.getCell(39).toString() : null);
				onlineRetailerInfoObj.setCategory((row.getCell(40) != null) ? row.getCell(40).toString() : null);
				onlineRetailerInfoObj.setSubCategory((row.getCell(41) != null) ? row.getCell(41).toString() : null);

				onlineRetailerInfolst.add(onlineRetailerInfoObj);
			}

			// Remove header
			if (!onlineRetailerInfolst.isEmpty())
			{
				onlineRetailerInfolst.remove(0);
			}
		}
		catch (FileNotFoundException exception)
		{

			LOG.info("Inside Utility : getCJDataFromXLSXFile : " + exception.getMessage());
			throw new CommissionJunctionBatchProcessException(exception);
		}
		catch (IOException exception)
		{
			LOG.info("Inside Utility : getCJDataFromXLSXFile : " + exception.getMessage());
			throw new CommissionJunctionBatchProcessException(exception);
		}
		catch (ParseException exception)
		{
			LOG.info("Inside Utility : getCJDataFromXLSXFile : " + exception.getMessage());
			throw new CommissionJunctionBatchProcessException(exception);
		}
		catch (Exception exception)
		{
			LOG.info("Inside Utility : getCJDataFromXLSXFile : " + exception.getMessage());
			throw new CommissionJunctionBatchProcessException(exception);
		}
		LOG.info("EXITING FROM getCJDataFromXLSXFile METHOD");
		return onlineRetailerInfolst;
	}

	@SuppressWarnings("unused")
	public static String formattedDate(String enteredDate) throws java.text.ParseException
	{
		final String methodName = "formattedDate";
		String cDate = null;
		if (null != enteredDate && !"".equals(enteredDate))
		{

			DateFormat oldFormatter = new SimpleDateFormat("dd-MMM-yyyy");
			DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

			Date convertedDate = (Date) oldFormatter.parse(enteredDate);
			cDate = formatter.format(convertedDate);
		}
		return cDate;
	}

	public static java.sql.Timestamp getFormattedDate() throws java.text.ParseException
	{
		final String methodName = "getFormattedDate";
		// LOG.info(ApplicationConstants.METHODSTART + methodName);

		final Date date = new Date();
		java.sql.Timestamp sqltDate = null;
		/*
		 * formatting the current date.
		 */
		// final DateFormat formater = new SimpleDateFormat("yyyy-dd-MM");
		final DateFormat formater = new SimpleDateFormat("MM-dd-yyyy");
		java.util.Date parsedUtilDate;
		parsedUtilDate = formater.parse(formater.format(date));
		sqltDate = new java.sql.Timestamp(parsedUtilDate.getTime());
		// LOG.info(ApplicationConstants.METHODEND + methodName);
		return sqltDate;
	}

	public static String formEmailBody(ArrayList<BatchProcessStatus> batchProcessStatusList, int numberFilesToProcess, int numberOfFilesProcessed,
			String totalTimeLeft, String totalTimeTaken, int batchNumber)
	{

		StringBuilder emailBody = new StringBuilder();
		emailBody.append("Hi,");
		emailBody.append("</br></br>");

		if (batchProcessStatusList != null)
		{

			if (batchProcessStatusList.isEmpty())
			{

				emailBody.append("Batch Process logs are not available for the date " + Utility.getCurrentDate() + ".\n");
				emailBody.append("Please Verify Batch processes.");
			}
			else
			{

				emailBody.append("Please Find the status of Commission Junction Batch Process.");
				emailBody.append("</br></br>");
				emailBody.append("<b>Batch Number:</b>" + batchNumber);
				emailBody.append("</br>");
				emailBody.append("<b>Time Taken:</b>" + totalTimeTaken);
				emailBody.append("</br>");
				emailBody.append("<b>Time Left:</b>" + totalTimeLeft);
				emailBody.append("</br>");
				emailBody.append("<b># Files Processed:</b>" + numberOfFilesProcessed);
				emailBody.append("</br>");
				emailBody.append("<b># Files Yet To Process:</b>" + numberFilesToProcess);
				emailBody.append("</br></br>");
				emailBody
						.append("<table cellspacing='0' cellpadding='0' border='1'><tr style='background-color: yellow;'><th>Batch Program Name</th><th>Excecution Date</th>");

				emailBody.append("<th># Rows Received</th>");
				emailBody.append("<th># Rows Processed</th>");
				emailBody.append("<th>Duplicates Count</th>");
				emailBody.append("<th>Expired Count</th>");
				emailBody.append("<th>Mandatory Fields Missing Count</th>");
				emailBody.append("<th>Existing Record Count</th>");
				emailBody.append("<th>UPC Length Count (Length<6)</th>");
				emailBody.append("<th>Status</th>");
				emailBody.append("<th>Reason</th>");
				emailBody.append("<th>ProcessedFileName</th>");

				emailBody.append("</tr>");

				for (int i = 0; i < batchProcessStatusList.size(); i++)
				{
					BatchProcessStatus batchProcessStatus = batchProcessStatusList.get(i);
					emailBody.append("<tr>");
					emailBody.append("<td>");
					emailBody.append(batchProcessStatus.getBatchProcessName());
					emailBody.append("</td>");
					emailBody.append("<td>");
					emailBody.append(batchProcessStatus.getExecutionDate());
					emailBody.append("</td>");
					emailBody.append("<td>");

					if (null != batchProcessStatus.getProcessedFileName() && !batchProcessStatus.getProcessedFileName().equals(""))
					{
						emailBody.append(batchProcessStatus.getRowsRecieved());

					}
					else
					{

						emailBody.append("0");
					}

					emailBody.append("</td>");
					emailBody.append("<td>");
					if (null != batchProcessStatus.getProcessedFileName() && !batchProcessStatus.getProcessedFileName().equals(""))
					{
						emailBody.append(batchProcessStatus.getRowsProcessed());
					}
					else
					{

						emailBody.append("0");
					}

					emailBody.append("</td>");

					emailBody.append("<td>");

					emailBody.append(batchProcessStatus.getDuplicatesCount());

					emailBody.append("</td>");

					emailBody.append("<td>");

					emailBody.append(batchProcessStatus.getExpiredCount());

					emailBody.append("</td>");

					emailBody.append("<td>");

					emailBody.append(batchProcessStatus.getMandatoryFieldsMissingCount());

					emailBody.append("</td>");

					emailBody.append("<td>");

					emailBody.append(batchProcessStatus.getExistingRecordCount());

					emailBody.append("</td>");
					emailBody.append("<td>");

					emailBody.append(batchProcessStatus.getInvalidRecordsCount());

					emailBody.append("</td>");

					if (null != batchProcessStatus.getStatus() && batchProcessStatus.getStatus().equals("1"))
					{
						emailBody.append("<td>");
						emailBody.append("Success");
						emailBody.append("</td>");
						emailBody.append("<td>");
						emailBody.append("N/A");
						emailBody.append("</td>");

					}
					else if (null != batchProcessStatus.getStatus() && batchProcessStatus.getStatus().equals("0"))
					{

						emailBody.append("<td>");
						emailBody.append("Failure");
						emailBody.append("</td>");
						emailBody.append("<td>");
						emailBody.append(batchProcessStatus.getReason());
						emailBody.append("</td>");
					}
					else
					{

						emailBody.append("<td>");
						emailBody.append("");
						emailBody.append("</td>");
						emailBody.append("<td>");
						emailBody.append("");
						emailBody.append("</td>");
					}

					emailBody.append("<td>");
					if (null != batchProcessStatus.getProcessedFileName() && !batchProcessStatus.getProcessedFileName().equals(""))
					{
						String renameFile = FilenameUtils.removeExtension(batchProcessStatus.getProcessedFileName());
						emailBody.append(renameFile);
					}
					else
					{

						emailBody.append("N/A");
					}
					emailBody.append("</td>");
					emailBody.append("</tr>");

				}
				emailBody.append("</table>");
			}
		}
		else
		{

			emailBody.append("Batch Process logs are not available for the date " + Utility.getCurrentDate() + ".\n");
			emailBody.append("Please Verify Batch processes.");
		}

		emailBody.append("</br></br>");
		emailBody.append("Regards</br>");
		emailBody.append("ScanSee Team");

		return emailBody.toString();
	}

	public static String getCurrentDate()
	{
		String currentDate = null;
		try
		{
			DateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
			Date date1 = new Date();
			currentDate = df.format(date1);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			// TODO: handle exception
		}
		return currentDate;
	}

	public static void unZipFiles(String source, String destination)
	{

		byte[] buffer = new byte[1024];

		try
		{

			GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(source));

			FileOutputStream out = new FileOutputStream(destination);

			int len;
			while ((len = gzis.read(buffer)) > 0)
			{
				out.write(buffer, 0, len);
			}

			gzis.close();
			out.close();

		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}

	}

	/*
	 * System.out.println("program name :" + nextLine[0]);
	 * System.out.println("setPrgUrl :" + nextLine[1]);
	 * System.out.println("setCatLogName :" + nextLine[2]);
	 * System.out.println("setLastUpdateDate :" + nextLine[3]);
	 * System.out.println("setRetStoreName :" + nextLine[4]);
	 * System.out.println("setDescription :" + nextLine[5]);
	 * System.out.println("setSku :" + nextLine[6]);
	 * System.out.println("setManufacturer :" + nextLine[7]);
	 * System.out.println("setManufacturerId :" + nextLine[8]);
	 * System.out.println("setUpc :" + nextLine[9]);
	 * System.out.println("setIsbn :" + nextLine[10]);
	 * System.out.println("setSalePrice :" + nextLine[11]);
	 * System.out.println("setCurrency :" + nextLine[12]);
	 * System.out.println("setPrice :" + nextLine[13]);
	 * System.out.println("setRetPrice :" + nextLine[14]);
	 * System.out.println("setFrmPrice :" + nextLine[15]);
	 * System.out.println("setFrmPrice :" + nextLine[16]);
	 * System.out.println("setBuyUrl :" + nextLine[17]);
	 * System.out.println("setImpressionUrl :" + nextLine[18]);
	 * System.out.println("setImageUrl :" + nextLine[19]);
	 * System.out.println("setAdvCategory :" + nextLine[20]);
	 * System.out.println("setThirdPartyId :" + nextLine[21]);
	 * System.out.println("setThirdPartyCat :" + nextLine[22]);
	 * System.out.println("setAuthor :" + nextLine[23]);
	 * System.out.println("setArtist :" + nextLine[24]);
	 * System.out.println("setTitle :" + nextLine[25]);
	 * System.out.println("setPublisher :" + nextLine[26]);
	 * System.out.println("setLabel :" + nextLine[27]);
	 * System.out.println("setSpecial :" + nextLine[28]);
	 * System.out.println("setGift :" + nextLine[29]);
	 * System.out.println("setPromotionalText :" + nextLine[30]);
	 * System.out.println("setStartDate :" + nextLine[31]);
	 * System.out.println("setEndDate :" + nextLine[32]);
	 * System.out.println("setOffline :" + nextLine[33]);
	 * System.out.println("setOnline :" + nextLine[34]);
	 * System.out.println("setInstock :" + nextLine[35]);
	 * System.out.println("setCondition :" + nextLine[36]);
	 * System.out.println("setWarranty :" + nextLine[37]);
	 * System.out.println("setShippingCost :" + nextLine[38]);
	 * System.out.println("setCategory :" + nextLine[39]); //
	 * System.out.println("setSubCategory :" + nextLine[40]);
	 */

	public static void generateStatusReportXLSX(ArrayList<CommissionJuncBatchStatus> statusList, String filePath) throws FileNotFoundException
	{

		try
		{
			Calendar calender = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			XSSFWorkbook wb_xssf = new XSSFWorkbook();
			int count = 1;
			XSSFSheet sheet = wb_xssf.createSheet("Comminssion Junction Batch Status");
			// Font setting for sheet.
			XSSFFont font = wb_xssf.createFont();
			font.setBoldweight((short) 700);
			sheet.setDefaultColumnWidth(30);
			// Create Styles for sheet.
			XSSFCellStyle headerStyle = wb_xssf.createCellStyle();
			headerStyle.setFillForegroundColor(LAVENDER.index);
			headerStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			headerStyle.setFont(font);
			XSSFCellStyle dataStyle = wb_xssf.createCellStyle();
			dataStyle.setWrapText(true);

			// Create Header Row
			XSSFRow headerRow = sheet.createRow(0);

			// Write row for header
			XSSFCell headerCell1 = headerRow.createCell(0);
			headerCell1.setCellStyle(headerStyle);
			headerCell1.setCellValue("File Name");
			XSSFCell headerCell2 = headerRow.createCell(1);
			headerCell2.setCellStyle(headerStyle);
			headerCell2.setCellValue("Rows Recieved");
			XSSFCell headerCell3 = headerRow.createCell(2);
			headerCell3.setCellStyle(headerStyle);
			headerCell3.setCellValue("Start Time");
			XSSFCell headerCell4 = headerRow.createCell(3);
			headerCell4.setCellStyle(headerStyle);
			headerCell4.setCellValue("End Time");
			XSSFCell headerCell5 = headerRow.createCell(4);
			headerCell5.setCellStyle(headerStyle);
			headerCell5.setCellValue("Total Time(sec)");
			XSSFCell headerCell6 = headerRow.createCell(5);
			headerCell6.setCellStyle(headerStyle);
			headerCell6.setCellValue("File Size(KB)");

			for (int i = 0; i < statusList.size(); i++)
			{
				CommissionJuncBatchStatus batchStatus = statusList.get(i);

				// Create First Data Row
				XSSFRow dataRow = sheet.createRow(count);

				dataRow.createCell(0).setCellValue(batchStatus.getFileName());
				dataRow.createCell(1).setCellValue(batchStatus.getRowsRecieved());
				dataRow.createCell(2).setCellValue(batchStatus.getStartTime());
				dataRow.createCell(3).setCellValue(batchStatus.getEndTime());
				dataRow.createCell(4).setCellValue(batchStatus.getTotalTimeInMs());
				dataRow.createCell(5).setCellValue(batchStatus.getFileSize());
				count++;

			}
			File file = new File(filePath);
			FileOutputStream fout = new FileOutputStream(file);
			wb_xssf.write(fout);
			fout.close();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static List<String> splitFile(String filePath, String fileName, int lines) throws CommissionJunctionBatchProcessException
	{
		final String NEWLINE = System.getProperty("line.separator");
		List<String> fileAfterSplit = new ArrayList<String>();
		try
		{
			// opens the file in a string buffer
			BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
			StringBuffer stringBuffer = new StringBuffer();

			// performs the splitting
			String line;
			int i = 0;
			int counter = 1;
			while ((line = bufferedReader.readLine()) != null)
			{
				stringBuffer.append(line);
				stringBuffer.append(NEWLINE);
				i++;
				if (i >= lines)
				{

					fileAfterSplit.add(fileName.substring(0, fileName.lastIndexOf(".")) + "_" + counter + ".txt");
					// saves the lines in the file
					saveFile(stringBuffer, fileName.substring(0, fileName.lastIndexOf(".")) + "_" + counter + ".txt");
					// creates a new buffer, so the old can get garbage
					// collected.
					stringBuffer = new StringBuffer();
					i = 0;
					counter++;
				}

			}
			if (i != 0 && stringBuffer.length() != 0)
			{
				fileAfterSplit.add(fileName.substring(0, fileName.lastIndexOf(".")) + "_" + counter + ".txt");
				// saves the lines in the file
				saveFile(stringBuffer, fileName.substring(0, fileName.lastIndexOf(".")) + "_" + counter + ".txt");
			}
			bufferedReader.close();
		}
		catch (IOException e)
		{
			LOG.error("Exception Occured in splitFile" + e);
			e.printStackTrace();
			throw new CommissionJunctionBatchProcessException("unable to read from file: " + fileName);
		}
		return fileAfterSplit;
	}

	private static void saveFile(StringBuffer stringBuffer, String filename) throws CommissionJunctionBatchProcessException
	{

		LOG.info("Save Split File:File Name" + filename);
		String path = PropertiesReader.getPropertyValue("cjdownloadpath");
		// Create File
		File file = new File(path + filename);
		FileWriter output = null;
		try
		{
			output = new FileWriter(file);
			output.write(stringBuffer.toString());
			LOG.info("Files After Split\n");
			LOG.info("file " + path + filename + " written");
			System.out.println("file " + path + filename + " written");
		}
		catch (IOException e)
		{

			LOG.error("Exception Occured in saveFile" + e);
			e.printStackTrace();
			throw new CommissionJunctionBatchProcessException(e.getMessage());
		}
		finally
		{

			try
			{
				output.close();
			}
			catch (IOException e)
			{
				LOG.error("Exception Occured in saveFile" + e);
				e.printStackTrace();
				throw new CommissionJunctionBatchProcessException(e.getMessage());
			}
		}

		LOG.info("Exit Split File");
	}

	public static String getTotalTimeLeft(long endTime, long startTime)
	{
		LOG.info("Inside getTotalTimeLeft");

		String totalTimeLeft;
		Long lngTimeLeft = endTime - startTime;
		Long timeLeftSeconds = lngTimeLeft / 1000;
		Long timeLeftMinutes = lngTimeLeft / (60 * 1000);
		int timeLeftHours;
		int timeLeftHrMin;
		if (timeLeftMinutes >= 60)
		{
			timeLeftHours = timeLeftMinutes.intValue() / 60;
			timeLeftHrMin = timeLeftMinutes.intValue() % 60;
			if (timeLeftHrMin != 0)
			{
				totalTimeLeft = String.valueOf(timeLeftHours) + " hrs " + timeLeftHrMin + " min";
			}
			else
			{
				totalTimeLeft = String.valueOf(timeLeftHours) + " hrs";
			}

		}
		else if (timeLeftMinutes >= 1)
		{
			totalTimeLeft = String.valueOf(timeLeftMinutes) + " min";
		}
		else
		{
			if (timeLeftSeconds < 0)
			{

				totalTimeLeft = "0 sec(Stopped Processing)";

			}
			else
			{
				totalTimeLeft = String.valueOf(timeLeftSeconds) + " sec";

			}

		}
		LOG.info("Exit getTotalTimeLeft");
		return totalTimeLeft;
	}

	public static String formatDate(Date date)
	{

		LOG.info("Inside formatDate");
		String formattedDate = null;
		if (null != date)
		{

			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

			formattedDate = dateFormat.format(date);
		}
		else
		{
			formattedDate = "";

		}
		LOG.info("Exit formatDate");
		return formattedDate;

	}
}
