package com.scansee.batch.common.pojos;

import java.util.ArrayList;

/**
 * The DealMapBatchStatus has setter an getter methods for deals.
 * @author sowjanya_d
 *
 */
public class CommissionJuncBatchStatus
{
	
	private String fileName;
	private String rowsRecieved;
	private String startTime;
	private String endTime;
	private String totalTimeInMs;
	private String fileSize;
	private int numberFilesToProcess;
	private String fileModifiedDate;
	public String getFileName()
	{
		return fileName;
	}
	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}
	public String getRowsRecieved()
	{
		return rowsRecieved;
	}
	public void setRowsRecieved(String rowsRecieved)
	{
		this.rowsRecieved = rowsRecieved;
	}

	public String getFileSize()
	{
		return fileSize;
	}
	public void setFileSize(String fileSize)
	{
		this.fileSize = fileSize;
	}
	public String getStartTime()
	{
		return startTime;
	}
	public void setStartTime(String startTime)
	{
		this.startTime = startTime;
	}
	public String getEndTime()
	{
		return endTime;
	}
	public void setEndTime(String endTime)
	{
		this.endTime = endTime;
	}
	public String getTotalTimeInMs()
	{
		return totalTimeInMs;
	}
	public void setTotalTimeInMs(String totalTimeInMs)
	{
		this.totalTimeInMs = totalTimeInMs;
	}
	public int getNumberFilesToProcess()
	{
		return numberFilesToProcess;
	}
	public void setNumberFilesToProcess(int numberFilesToProcess)
	{
		this.numberFilesToProcess = numberFilesToProcess;
	}
	public String getFileModifiedDate()
	{
		return fileModifiedDate;
	}
	public void setFileModifiedDate(String fileModifiedDate)
	{
		this.fileModifiedDate = fileModifiedDate;
	}

	
	
	
}
