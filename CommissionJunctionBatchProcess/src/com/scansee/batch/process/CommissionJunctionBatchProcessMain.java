package com.scansee.batch.process;

import java.util.ArrayList;
import java.util.Calendar;
import org.apache.log4j.Logger;
import com.scansee.batch.exception.CommissionJunctionBatchProcessException;
import com.scansee.batch.service.CommissionBatchService;
import com.scansee.batch.serviceImpl.CommissionBatchServiceImpl;
import com.scansee.externalapi.common.pojos.ExternalAPIVendor;

/**
 * This class is used to run commission junction batch process.
 * 
 * @author shyamsundara_hm
 */
public class CommissionJunctionBatchProcessMain
{

	/**
	 * To get logger instance.
	 */
	private static final Logger LOG = Logger.getLogger(CommissionJunctionBatchProcessMain.class);

	/**
	 * constructor for FetchAndProcessData.
	 */
	private CommissionJunctionBatchProcessMain()
	{

	}

	/**
	 * This main method is used to run commission junction batch process.
	 * 
	 * @param args
	 * @throws CommissionJunctionBatchProcessException
	 */

	public static void main(String[] args) throws CommissionJunctionBatchProcessException

	{
		CommissionBatchService commissionBatchServiceObj = new CommissionBatchServiceImpl();
		ArrayList<ExternalAPIVendor> objExternalAPIVendorList = new ArrayList<ExternalAPIVendor>();
		String apiPartnerID = null;

		try
		{
			LOG.info("*********************************************************************");
			LOG.info("Commission junction  Process Start @:" + Calendar.getInstance().getTime());

			// Calling service layer method to fetch api patner id of commission
			// junction api.
			objExternalAPIVendorList = commissionBatchServiceObj.getAPIList("Products");

			for (int i = 0; i < objExternalAPIVendorList.size(); i++)
			{
				if (objExternalAPIVendorList.get(i).getVendorName().equalsIgnoreCase("CommissionJunction"))
				{
					apiPartnerID = objExternalAPIVendorList.get(i).getApiPartnerID();
				}
			}
			// calling service layer method to move commission junction data to
			// staging table.
			commissionBatchServiceObj.commissionBatchProcess(apiPartnerID);
		}

		catch (CommissionJunctionBatchProcessException exception)
		{
			LOG.error("Exception occured in CommissionJunctionBatchProcessMain while fetching" + exception);
			commissionBatchServiceObj.insertCJBatchStatus(0, exception.getMessage(), Calendar.getInstance().getTime(), apiPartnerID);
			commissionBatchServiceObj.sendCJBatchProcessStatus(null, 0, 0, "0", "0", 0);

		}
		catch (Exception exception)
		{
			LOG.error("Exception occured in CommissionJunctionBatchProcessMain while fetching" + exception);
		}
		LOG.info("Commission junction Process Ends  @:" + Calendar.getInstance().getTime());
		LOG.info("######################################################################");
	}

}