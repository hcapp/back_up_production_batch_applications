package com.scansee.batch.serviceImpl;

import it.sauronsoftware.ftp4j.FTPAbortedException;
import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferException;
import it.sauronsoftware.ftp4j.FTPException;
import it.sauronsoftware.ftp4j.FTPFile;
import it.sauronsoftware.ftp4j.FTPIllegalReplyException;
import it.sauronsoftware.ftp4j.FTPListParseException;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.batch.common.Constants;
import com.scansee.batch.common.PropertiesReader;
import com.scansee.batch.common.Utility;
import com.scansee.batch.common.pojos.AppConfiguration;
import com.scansee.batch.common.pojos.BatchProcessStatus;
import com.scansee.batch.common.pojos.CommissionJuncBatchStatus;
import com.scansee.batch.common.pojos.EmailComponent;
import com.scansee.batch.common.pojos.OnlineRetailerInfo;
import com.scansee.batch.dao.CommissionBatchDao;
import com.scansee.batch.daoImpl.CommissionBatchDaoImpl;
import com.scansee.batch.exception.CommissionJunctionBatchProcessException;
import com.scansee.batch.service.CommissionBatchService;
import com.scansee.externalapi.common.pojos.ExternalAPIVendor;

public class CommissionBatchServiceImpl implements CommissionBatchService
{

	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(CommissionBatchServiceImpl.class);

	/**
	 * Instance variable for batch process DAO instance.
	 */
	CommissionBatchDao commissionBatchDaoObj = new CommissionBatchDaoImpl();

	// CommissionBatchService commissionBatchServiceObj = new
	// CommissionBatchServiceImpl();
	/**
	 * This method is used to fetch latest file from FTP site and parse csv file
	 * and calls method in DAO layer to insert data.
	 */
	@Override
	public String commissionBatchProcess(String apiPartnerID) throws CommissionJunctionBatchProcessException
	{
		final String methodName = " commissionBatchProcess";
		LOG.info(Constants.METHODSTART + methodName);
		String response = null;
		String batchStatusFilePath = PropertiesReader.getPropertyValue("cjdownloadpath") + "CJ_BatchStatus.xlsx";
		int numberFilesToProcess = 0;
		int numberOfFilesProcessed = 0;
		int batchNumber = 1;
		String totalTimeLeft = null;
		String totalTimeTaken = null;
		Date startTime = null;
		String strbatchStartTime = null;
		try
		{
			// CommissionBatchService commissionBatchServiceObj = new
			// CommissionBatchServiceImpl();
			// processCJFTPFiles("Boscov_s_Department_Stores-Product_Catalog.txt",
			// apiPartnerID, commissionBatchServiceObj,1);
			SimpleDateFormat formatter = new SimpleDateFormat("HH.mm.ss");

			String strBatchEndTime = PropertiesReader.getPropertyValue("batchEndTime");

			Date batchEndTime = formatter.parse(strBatchEndTime);
			strbatchStartTime = formatter.format(new Date());
			startTime = formatter.parse(strbatchStartTime);
			while (startTime.before(batchEndTime))
			{
				LOG.info("Start of Batch Number:" + batchNumber);
				LOG.info("Start Time:" + String.valueOf(formatter.format(startTime)));
				ArrayList<CommissionJuncBatchStatus> statusList = getCommissionJunctionFileFromFtp(apiPartnerID, batchNumber);
				Utility.generateStatusReportXLSX(statusList, batchStatusFilePath);
				if (statusList != null && !statusList.isEmpty())
				{
					numberFilesToProcess = statusList.get(0).getNumberFilesToProcess();
					numberOfFilesProcessed = statusList.size();
				}

				LOG.info("End Time:" + String.valueOf(formatter.format(new Date())));
				LOG.info("End of Batch Number:" + batchNumber);

				totalTimeLeft = Utility.getTotalTimeLeft(batchEndTime.getTime(), formatter.parse(formatter.format(new Date())).getTime());
				totalTimeTaken = Utility.getTotalTimeLeft(formatter.parse(formatter.format(new Date())).getTime(), startTime.getTime());
				sendCJBatchProcessStatus(batchStatusFilePath, numberFilesToProcess, numberOfFilesProcessed, totalTimeLeft, totalTimeTaken,
						batchNumber);

				batchNumber++;
				Thread.sleep(60000 * 2);
				strbatchStartTime = formatter.format(new Date());
				startTime = formatter.parse(strbatchStartTime);
			}

		}
		catch (Exception exception)
		{
			LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
			exception.printStackTrace();
			throw new CommissionJunctionBatchProcessException(exception);
		}
		LOG.info(Constants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method is used to connect FTP server and down load latest commission
	 * junction file.
	 * 
	 * @return Ftp file
	 * @throws CommissionJunctionBatchProcessException
	 */
	@SuppressWarnings("unused")
	public ArrayList<CommissionJuncBatchStatus> getCommissionJunctionFileFromFtp(String apiPartnerID, int batchNumber)
			throws CommissionJunctionBatchProcessException
	{

		FTPClient client = new FTPClient();
		// FileOutputStream fos = null;
		ArrayList<FTPFile> ftpFileslst = new ArrayList<FTPFile>();
		FTPFile downloadfile = null;
		String downloadPath = null;
		String fileName = null;
		String unzipFileName = null;
		String response = null;
		int numberofFilesInFtp = 0;
		int reply;
		int batchSize = 0;
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		long starttime;
		long endTime;
		long totlaTime;
		int rowsRecieved;
		CommissionBatchService commissionBatchServiceObj = new CommissionBatchServiceImpl();
		ArrayList<CommissionJuncBatchStatus> statusList = new ArrayList<CommissionJuncBatchStatus>();
		downloadPath = PropertiesReader.getPropertyValue("cjdownloadpath");
		try
		{

			ArrayList<AppConfiguration> batchSizeConf = commissionBatchDaoObj.getAppConfig(Constants.BATCHSIZE);

			for (int j = 0; j < batchSizeConf.size(); j++)
			{
				if (batchSizeConf.get(j).getScreenName().equals(Constants.BATCHSIZE))
				{
					batchSize = Integer.valueOf(batchSizeConf.get(j).getScreenContent());
				}

			}

			// Get FTP connection by providing ftp credentials.
			client = getFTPConnection();

			client.setAutoNoopTimeout(10000);
			// change the ftp default directory to commission batch files active
			// file directory
			// client.setKeepAlive(true);
			client.changeDirectory(PropertiesReader.getPropertyValue("cjftppath"));
			FTPFile[] files = client.list();

			ArrayUtils.reverse(files);

			numberofFilesInFtp = files.length;
			// Adding ftp file with extension gz to ftp file list.
			for (FTPFile ftpFile : files)
			{
				String filename = ftpFile.getName();

				if (FilenameUtils.isExtension(filename, "gz"))
				{
					ftpFileslst.add(ftpFile);
					if (ftpFileslst.size() == batchSize)
					{
						break;
					}
				}
			}

			if (ftpFileslst != null && !ftpFileslst.isEmpty())
			{

				for (FTPFile ftpfile : ftpFileslst)
				{
					downloadfile = ftpfile;
					if (null != downloadfile)
					{
						double filesize = downloadfile.getSize();
						Double filesizeInKB = filesize / 1024;
						Double filesizeInMB = filesize / 1048576;

						if (filesizeInMB <= 500)
						{
							fileName = downloadfile.getName();

							File obj = new File(downloadPath);
							if (!obj.exists())
							{
								obj.mkdir();
							}
							client.download(fileName, new File(downloadPath + fileName));
							System.out.println("Downloaded File:" + fileName);
							System.out.println("File Modified Date:" + Utility.formatDate(downloadfile.getModifiedDate()));
							LOG.info("Downloaded File:" + fileName);
						}
					}
				}
				LOG.info("Disconnect FTP after completing download");
				client.disconnect(true);
				for (FTPFile ftpfile : ftpFileslst)
				{
					unzipFileName = ftpfile.getName() + ".txt";
					fileName = ftpfile.getName();
					Utility.unZipFiles(downloadPath + fileName, downloadPath + unzipFileName);

					try
					{
						CommissionJuncBatchStatus batchStatus = new CommissionJuncBatchStatus();
						starttime = System.currentTimeMillis();
						double filesize = ftpfile.getSize();
						Double filesizeInKB = filesize / 1024;
						Double filesizeInMB = filesize / 1048576;
						batchStatus.setFileName(ftpfile.getName());
						batchStatus.setFileModifiedDate(Utility.formatDate(ftpfile.getModifiedDate()));
						batchStatus.setFileSize(String.valueOf(Math.ceil(filesizeInKB)));
						System.out.println("Start Time:" + String.valueOf(sdf.format(date)));
						batchStatus.setStartTime(String.valueOf(sdf.format(date)));
						rowsRecieved = processCJFTPFiles(unzipFileName, apiPartnerID, commissionBatchServiceObj, batchNumber);
						date = new Date();
						System.out.println("End Time:" + String.valueOf(sdf.format(date)));
						batchStatus.setEndTime(String.valueOf(sdf.format(date)));
						endTime = System.currentTimeMillis();
						totlaTime = (endTime - starttime) / 1000;
						batchStatus.setTotalTimeInMs(String.valueOf(totlaTime));
						batchStatus.setRowsRecieved(String.valueOf(rowsRecieved));
						statusList.add(batchStatus);

					}
					catch (CommissionJunctionBatchProcessException exception)
					{
						LOG.info("Error occured CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);

						commissionBatchServiceObj.insertCJBatchStatus(0, "File Name:" + fileName + "\n" + exception.getMessage(), Calendar
								.getInstance().getTime(), apiPartnerID);

						continue;

					}
				}

			}
			else
			{

				LOG.info("No file to process");
				response = "No file to process";
				commissionBatchServiceObj.insertCJBatchStatus(0, response, Calendar.getInstance().getTime(), apiPartnerID);
			}

		}

		catch (SocketException exception)
		{
			LOG.error("Error Occued id getCommissionJunctionFileFromFtp: File Name:");
			exception.printStackTrace();
			LOG.info("Error occured CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
			throw new CommissionJunctionBatchProcessException(exception);
		}
		catch (IOException exception)
		{
			LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
			exception.printStackTrace();
			commissionBatchServiceObj.insertCJBatchStatus(0, "File Name:" + fileName + "\n" + exception.getMessage(), Calendar.getInstance()
					.getTime(), apiPartnerID);

			getCommissionJunctionFileFromFtp(apiPartnerID, batchNumber);
			// throw new CommissionJunctionBatchProcessException(exception);
		}
		catch (IllegalStateException exception)
		{
			LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
			exception.printStackTrace();
			throw new CommissionJunctionBatchProcessException(exception);
		}

		catch (FTPIllegalReplyException exception)
		{
			LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
			exception.printStackTrace();
			throw new CommissionJunctionBatchProcessException(exception);
		}

		catch (FTPException exception)
		{
			LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
			exception.printStackTrace();
			throw new CommissionJunctionBatchProcessException(exception);
		}
		catch (FTPDataTransferException exception)
		{
			LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
			exception.printStackTrace();
			throw new CommissionJunctionBatchProcessException(exception);
		}
		catch (FTPAbortedException exception)
		{
			LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
			exception.printStackTrace();
			throw new CommissionJunctionBatchProcessException(exception);
		}
		catch (FTPListParseException exception)
		{
			LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
			exception.printStackTrace();
			throw new CommissionJunctionBatchProcessException(exception);
		}

		finally
		{
			// fos.close();
			try
			{
				if (client.isConnected())
				{
					client.disconnect(true);
				}

			}
			catch (IOException exception)
			{
				LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
				exception.printStackTrace();
				throw new CommissionJunctionBatchProcessException(exception);
			}
			catch (IllegalStateException exception)
			{
				LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
				exception.printStackTrace();
				throw new CommissionJunctionBatchProcessException(exception);
			}
			catch (FTPIllegalReplyException exception)
			{
				LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
				exception.printStackTrace();
				throw new CommissionJunctionBatchProcessException(exception);
			}
			catch (FTPException exception)
			{
				LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
				exception.printStackTrace();
				throw new CommissionJunctionBatchProcessException(exception);
			}

		}

		if (statusList != null && !statusList.isEmpty())
		{
			if (numberofFilesInFtp > 0)
			{

				numberofFilesInFtp = numberofFilesInFtp - statusList.size();
				statusList.get(0).setNumberFilesToProcess(numberofFilesInFtp);

			}

		}

		return statusList;

	}

	/**
	 * Works on a single file system entry and calls itself recursively if it
	 * turns out to be a directory.
	 * 
	 * @param file
	 *            A file or a directory to process
	 */
	public static File traverse(File files)
	{
		// Print the name of the entry
		File file = null;
		File[] files2 = files.listFiles();
		if (files2.length > 0)
		{
			file = files2[0];
			// Check if it is a directory
			for (File ftpFile : files2)
			{

				// Date date1 = new Date(file.lastModified());
				// Date date2 = new Date(ftpFile.lastModified());
				// java.util.Date date2 = ftpFile.getTimestamp().getTime();

				String filename = ftpFile.getName();

				if (FilenameUtils.isExtension(filename, "txt"))
				{
					file = ftpFile;

					/*
					 * if (date1.compareTo(date2) < 0) { file = ftpFile; }
					 */

				}

			}
		}
		return file;

	}

	/**
	 * This method is used to move commission junction data from staging table
	 * to production table.
	 * 
	 * @return String containing response message
	 * @throws CommissionJunctionBatchProcessException
	 */
	@Override
	public String movingCJDataToProduction(String latestftpfile, int batchNumber) throws CommissionJunctionBatchProcessException
	{
		LOG.info("Inside movingCJDataToProduction");
		String response = null;
		try
		{
			response = commissionBatchDaoObj.CJDataPortingUPC(latestftpfile, batchNumber);
			if (response.equals("SUCCESS"))
			{
				response = commissionBatchDaoObj.CJDataPortingISBN(latestftpfile);
				if (response.equals("SUCCESS"))
				{
					response = commissionBatchDaoObj.CJDataPortingProductName(latestftpfile);

				}
			}
		}
		catch (CommissionJunctionBatchProcessException exception)
		{
			LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
			exception.printStackTrace();
			throw new CommissionJunctionBatchProcessException(exception);
		}
		LOG.info("Exit movingCJDataToProduction");
		return response;
	}

	/**
	 * This method is used to update commission junction batch process.
	 * 
	 * @return String containing response message
	 * @throws CommissionJunctionBatchProcessException
	 */
	@Override
	public String updateCJBatchStatus(int status, String reason, Date date, String apiName, String filename)
			throws CommissionJunctionBatchProcessException
	{
		final String methodName = "updateCJBatchStatus";
		LOG.info(Constants.METHODSTART + methodName);
		String response = commissionBatchDaoObj.updateCJBatchStatus(status, reason, date, apiName, filename);
		LOG.info(Constants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method is used to insert commission junction batch status.
	 * 
	 * @return String containing response message
	 * @throws CommissionJunctionBatchProcessException
	 */
	@Override
	public String insertCJBatchStatus(int statusMessage, String reason, Date date, String apiName) throws CommissionJunctionBatchProcessException
	{
		final String methodName = "insertBatchStatus";
		LOG.info(Constants.METHODSTART + methodName);
		String response = commissionBatchDaoObj.insertCJBatchStatus(statusMessage, reason, date, apiName);
		LOG.info(Constants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method is used to fetch commission junction api patner id.
	 * 
	 * @return String containing response message
	 * @throws CommissionJunctionBatchProcessException
	 */
	@Override
	public ArrayList<ExternalAPIVendor> getAPIList(String moduleName) throws CommissionJunctionBatchProcessException
	{
		ArrayList<ExternalAPIVendor> objExternalAPIVendor = new ArrayList<ExternalAPIVendor>();
		objExternalAPIVendor = commissionBatchDaoObj.getExternalAPIList(moduleName);
		return objExternalAPIVendor;
	}

	/**
	 * This method is used to get ftp connection.
	 */
	public static FTPClient getFTPConnection() throws CommissionJunctionBatchProcessException
	{
		LOG.info("Inside getFTPConnection method");
		FTPClient client = new FTPClient();
		int reply;
		try
		{
			client.connect(PropertiesReader.getPropertyValue("ftp_url"));
			client.login(PropertiesReader.getPropertyValue("ftp_username"), PropertiesReader.getPropertyValue("ftp_password"));
			LOG.info("***FTP connection Established*****");
			client.setType(FTPClient.TYPE_BINARY);

		}
		catch (SocketException exception)
		{
			LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
			exception.printStackTrace();
			throw new CommissionJunctionBatchProcessException(exception);
		}
		catch (IOException exception)
		{
			LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
			exception.printStackTrace();
			throw new CommissionJunctionBatchProcessException(exception);
		}
		catch (IllegalStateException e)
		{
			e.printStackTrace();
			LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + e);
			e.printStackTrace();
			throw new CommissionJunctionBatchProcessException(e);

		}
		catch (FTPIllegalReplyException e)
		{

			e.printStackTrace();
			LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + e);
			throw new CommissionJunctionBatchProcessException(e);

		}
		catch (FTPException e)
		{
			e.printStackTrace();
			LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + e);
			throw new CommissionJunctionBatchProcessException(e);

		}

		return client;
	}

	public static ArrayList<String> moveProcessedFile(String latestFile) throws CommissionJunctionBatchProcessException
	{

		LOG.info("Inside Service Method moveProcessedFile");
		FTPClient client = new FTPClient();
		String processedFilePath = null;
		String activeFilePath = null;
		Date date = new Date();
		String processedFile = null;
		try
		{
			client = getFTPConnection();
			processedFilePath = PropertiesReader.getPropertyValue("processedcjfilepath");
			activeFilePath = PropertiesReader.getPropertyValue("cjftppath");
			// client.changeWorkingDirectory(PropertiesReader.getPropertyValue("cjpath"));
			if (null != latestFile && !"".equals(latestFile))
			{

				processedFile = FilenameUtils.removeExtension(latestFile);
				String renameFile = FilenameUtils.removeExtension(processedFile) + "_" + date.getTime() + ".gz";
				client.rename(activeFilePath + "/" + processedFile, processedFilePath + renameFile);

				LOG.info("Successfully moved process file::" + processedFile);

			}

		}
		catch (SocketException exception)
		{
			LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
			exception.printStackTrace();
			throw new CommissionJunctionBatchProcessException(exception);
		}
		catch (IOException exception)
		{
			LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
			exception.printStackTrace();
			throw new CommissionJunctionBatchProcessException(exception);
		}
		catch (IllegalStateException e)
		{

			e.printStackTrace();
			LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + e);

			throw new CommissionJunctionBatchProcessException(e);
		}
		catch (FTPIllegalReplyException e)
		{

			e.printStackTrace();
			LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + e);

			throw new CommissionJunctionBatchProcessException(e);
		}
		catch (FTPException e)
		{

			e.printStackTrace();
			LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + e);
			throw new CommissionJunctionBatchProcessException(e);
		}
		finally
		{

			try
			{
				client.disconnect(true);

			}
			catch (IOException exception)
			{
				LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
				exception.printStackTrace();
				throw new CommissionJunctionBatchProcessException(exception);
			}
			catch (IllegalStateException exception)
			{

				exception.printStackTrace();
				LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
				exception.printStackTrace();
				throw new CommissionJunctionBatchProcessException(exception);
			}
			catch (FTPIllegalReplyException exception)
			{

				exception.printStackTrace();
				LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
				throw new CommissionJunctionBatchProcessException(exception);
			}
			catch (FTPException exception)
			{

				exception.printStackTrace();
				LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
				throw new CommissionJunctionBatchProcessException(exception);
			}
		}
		LOG.info("Exit Service Method moveProcessedFile");
		return null;
	}

	public String sendCJBatchProcessStatus(String attachFilePath, int numberFilesToProcess, int numberOfFilesProcessed, String timeLeft,
			String timeTaken, int batchNumber) throws CommissionJunctionBatchProcessException
	{
		LOG.info("Inside EmailNotificationServiceImpl : getBatchProcessStatus ");
		ArrayList<BatchProcessStatus> batchProcessStatusList = null;
		ArrayList<AppConfiguration> appConfigurations = null;
		String smtpHost = null;
		String smtpPort = null;
		String emailrecipients[];
		String response = null;
		try
		{
			batchProcessStatusList = commissionBatchDaoObj.getBatchProcessStatus("CommissionJunction", batchNumber);
			appConfigurations = commissionBatchDaoObj.getAppConfig(Constants.EMAIL);
			String mailContent = Utility.formEmailBody(batchProcessStatusList, numberFilesToProcess, numberOfFilesProcessed, timeLeft, timeTaken,
					batchNumber);
			ArrayList<AppConfiguration> emailConf = commissionBatchDaoObj.getAppConfig(Constants.EMAILCONFIG);

			for (int j = 0; j < emailConf.size(); j++)
			{
				if (emailConf.get(j).getScreenName().equals(Constants.SMTPHOST))
				{
					smtpHost = emailConf.get(j).getScreenContent();
				}
				if (emailConf.get(j).getScreenName().equals(Constants.SMTPPORT))
				{
					smtpPort = emailConf.get(j).getScreenContent();
				}
			}

			if (appConfigurations != null && !appConfigurations.isEmpty())
			{
				emailrecipients = appConfigurations.get(0).getScreenContent().split(",");
				EmailComponent.multipleUsersmailingComponent("support@scansee.com", emailrecipients, "CommissionJunction Batch Process Status",
						mailContent, smtpHost, smtpPort, attachFilePath);

			}
			else
			{
				response = "Email recipients are not available. Please Configure";
			}

		}

		catch (MessagingException exception)
		{
			exception.printStackTrace();
			LOG.info("Inside CommissionBatchServiceImpl : sendCJBatchProcessStatus : " + exception);
			throw new CommissionJunctionBatchProcessException(exception);
		}
		catch (CommissionJunctionBatchProcessException exception)
		{
			exception.printStackTrace();
			LOG.info("Inside CommissionBatchServiceImpl : sendCJBatchProcessStatus : " + exception);
			throw new CommissionJunctionBatchProcessException(exception);
		}
		return response;
	}

	public int processCJFTPFiles(String fileName, String apiPartnerID, CommissionBatchService commissionBatchServiceObj, int batchNumber)
			throws CommissionJunctionBatchProcessException
	{
		final String methodName = "processCJFTPFiles";
		LOG.info(Constants.METHODSTART + methodName);

		String cjdownloadpath = PropertiesReader.getPropertyValue("cjdownloadpath");
		String latftpfile = null;

		ArrayList<OnlineRetailerInfo> onlineRetailerlst = null;
		String response = null;
		int status;
		int rowsRecieved = 0;
		try
		{
			if (fileName != null)
			{
				if (FilenameUtils.isExtension(fileName, "txt"))
				{
					latftpfile = fileName;

					LOG.info("Recieved Commission Junction File Name:" + latftpfile);
					File file = new File(cjdownloadpath + latftpfile);

					// calling utility method to read last modified excel
					// file.

					List<String> fileAfterSplit = Utility.splitFile(cjdownloadpath + latftpfile, latftpfile, 20000);

					if (!fileAfterSplit.isEmpty())
					{
						for (int i = 0; i < fileAfterSplit.size(); i++)
						{

							try
							{
								onlineRetailerlst = Utility.getOnlineRetailerList(new File(cjdownloadpath + fileAfterSplit.get(i)), i);
								if (onlineRetailerlst != null && !onlineRetailerlst.isEmpty())
								{
									// Refreshing staging table before inserting
									// commission
									// junction data
									rowsRecieved = rowsRecieved + onlineRetailerlst.size();

									commissionBatchDaoObj.RefreshCJData();
									// file

									// Inserting staging table data
									commissionBatchDaoObj.insertCommissionDataToStagingTable(onlineRetailerlst, fileAfterSplit.get(i));

									response = commissionBatchServiceObj.movingCJDataToProduction(fileAfterSplit.get(i), batchNumber);

									LOG.info("Response From DAO layer for File:" + fileAfterSplit.get(i) + ":" + response);

									if (null != response && response.equals("SUCCESS"))
									{
										status = 1;
									}
									else
									{
										status = 0;
									}

									commissionBatchServiceObj.updateCJBatchStatus(status, response, Calendar.getInstance().getTime(), apiPartnerID,
											fileAfterSplit.get(i));

								}
								else
								{
									LOG.info("Empty file");
								}

							}
							catch (CommissionJunctionBatchProcessException exception)
							{
								commissionBatchServiceObj.insertCJBatchStatus(0,
										"File Name:" + fileAfterSplit.get(i) + "\n" + exception.getMessage(), Calendar.getInstance().getTime(),
										apiPartnerID);

								continue;
							}

						}
						//moveProcessedFile(latftpfile);
					}

				}
				// LOG.info("No text file to process .");

			}
			else
			{

				LOG.info("No file to process");
				response = "No file to process";
				commissionBatchServiceObj.insertCJBatchStatus(0, response, Calendar.getInstance().getTime(), apiPartnerID);
			}

		}
		catch (IOException exception)
		{
			LOG.info("Inside CommissionBatchServiceImpl : getCommissionJunctionFileFromFtp : " + exception);
			exception.printStackTrace();
			throw new CommissionJunctionBatchProcessException(exception);
		}
		LOG.info(Constants.METHODEND + methodName);
		return rowsRecieved;
	}

	/*
	 * public static void main(String[] args) { File files = new
	 * File("D:/CommissionJunction/"); traverse(files); String latestfile =
	 * "SmoothFitness_com-Smooth_Fitness_Product_Catalog.txt.gz.txt"; String
	 * renameFile = FilenameUtils.removeExtension(latestfile);
	 * System.out.println("After Renaming" + renameFile); }
	 */
}
