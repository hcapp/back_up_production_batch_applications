package com.hubciti.pushnotification.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hubciti.pushnotification.common.helper.Constants;
import com.hubciti.pushnotification.common.helper.PayLoadGenerator;
import com.hubciti.pushnotification.common.helper.RSSFeedParser;
import com.hubciti.pushnotification.common.helper.SortRSSFeedByDate;
import com.hubciti.pushnotification.common.pojo.Configuration;
import com.hubciti.pushnotification.common.pojo.Data;
import com.hubciti.pushnotification.common.pojo.DeviceId;
import com.hubciti.pushnotification.common.pojo.NotificationDetails;
import com.hubciti.pushnotification.common.pojo.RSSFeed;
import com.hubciti.pushnotification.common.pojo.RSSFeedMessage;
import com.hubciti.pushnotification.exception.PushNotificationException;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsNotification;
import com.notnoop.apns.ApnsService;

/*---------------------------------------------------------------
 * PushNotificationHelper class for fetching news from specified RSSFeed URI, Configuration 
 * & Sending notification to different server(APNS,GCM and FCM) 
 * 
 *  @Author : Kirankumar Garaddi
 *  @written : 7/12/2015
 * 
 * Compilation : PushNotificationHelper.java
 * Execution   : PushNotificationHelper
 *     
 * 
 ----------------------------------------------------------------*/
public class PushNotificationHelper {

	private static final Logger LOG = LoggerFactory.getLogger(PushNotificationHelper.class);

	/**
	 * Method To Parse RSS feed
	 * 
	 * @param url
	 * @return {@link RSSFeed} object.
	 */
	public static RSSFeed parseRSSFeed(String url) {
		LOG.info("Method Start: parseRSSFeed(), Class: PushNotificationHelper");
		RSSFeedParser parser = new RSSFeedParser(url);
		RSSFeed feed = parser.readFeed();
		LOG.info("Method End: parseRSSFeed(), Class: PushNotificationHelper");
		return feed;
	}

	/**
	 * Method to get RSS feed in sorted by limiting the number of messages
	 * Sorting is done by date.
	 * 
	 * @param url
	 *            - URL for RSS feed
	 * @param noOfMsg
	 *            - Number of messages required form RSS Feed (If null, all
	 *            message will be sent)
	 * @param type
	 *            - type of message (General news, Sports news)
	 * @return {@link RSSFeed} object.
	 */
	public static RSSFeed getSortedRSSFeedByDate(String url, Integer noOfMsg, String type) {
		LOG.info("Method Start: getSortedRSSFeedByDate(), Class: PushNotificationHelper");
		if (null != noOfMsg && noOfMsg <= 0) {
			return null;
		}

		Integer listSize = 0;
		RSSFeed respFeed = null;
		/*
		 * Get RSS Feed
		 */
		RSSFeed feed = parseRSSFeedType2(url);
		if (null != feed) {
			List<RSSFeedMessage> feedMsgList = feed.getRssFeedMsg();
			if (null != feedMsgList && !feedMsgList.isEmpty()) {
				listSize = feedMsgList.size();
				Integer loopCount = 0;
				if (null != noOfMsg) {
					loopCount = (noOfMsg < listSize) ? noOfMsg : listSize;
					;
				} else {
					loopCount = listSize;
				}
				RSSFeedMessage objFeedMsg = null;

				/*
				 * Sorting RSS feed by date, latest new first.
				 */
				SortRSSFeedByDate sortByDate = new SortRSSFeedByDate();
				Collections.sort(feedMsgList, sortByDate);

				/*
				 * Limiting the number of news sent.
				 */
				respFeed = new RSSFeed(feed.getTitle(), feed.getLink(), feed.getDescription());
				for (int i = 0; i < loopCount; i++) {
					objFeedMsg = new RSSFeedMessage();
					objFeedMsg = feedMsgList.get(i);
					objFeedMsg.setType(type);
					respFeed.getRssFeedMsg().add(objFeedMsg);
				}
			}
		}
		LOG.info("Method End: getSortedRSSFeedByDate(), Class: PushNotificationHelper");
		return respFeed;
	}

	/**
	 * Method Reads RSSFeed from URL and arrange each message separated by
	 * specified string.
	 * 
	 * @return {@link RSSFeedMessage} object.
	 */
	public static RSSFeedMessage getRSSFeedNews(String feedURL,String news_type,int number) {
		LOG.info("Method Start: getRSSFeedNews(), Class: PushNotificationHelper");
		/*
		 * Parse RSS Feeds and insert to database
		 */
		RSSFeed responseRssFeed = new RSSFeed("HubCiti News", null, null);
		RSSFeed rssFeed = null;
		RSSFeedMessage feedMsg = null;
		String title = "";
		String link = "";
		String pubDate = "";
		String type = "";
		String imagePath = "";
		String mediaText = "";
		String splitTxt = Constants.SPLIT_RSSFEED_NEWS;

		/*
		 * get sorted RSS Feeds by Date
		 */
		rssFeed = PushNotificationHelper.getSortedRSSFeedByDate(feedURL, number, news_type);
		for (RSSFeedMessage message : rssFeed.getRssFeedMsg()) {
			responseRssFeed.getRssFeedMsg().add(message);
		}

		/*
		 * 
		 * Appending string |~~| in the end of each message to differentiate.
		 */
		Boolean firstEntry = true;
		if (responseRssFeed.getRssFeedMsg() != null && !responseRssFeed.getRssFeedMsg().isEmpty()) {
			feedMsg = new RSSFeedMessage();
			for (RSSFeedMessage msg : responseRssFeed.getRssFeedMsg()) {
				if (firstEntry) {
					title = msg.getTitle();
					link = msg.getLink();
					pubDate = msg.getPubDate();
					type = msg.getType();
					imagePath = msg.getImgPath();
					mediaText = msg.getMediaText();
					firstEntry = false;
				} else {
					title = title + splitTxt + msg.getTitle();
					link = link + splitTxt + msg.getLink();
					pubDate = pubDate + splitTxt + msg.getPubDate();
					type = type + splitTxt + msg.getType();
					imagePath = imagePath + splitTxt + msg.getImgPath();
					mediaText = mediaText + splitTxt + msg.getMediaText();
				}
			}
			feedMsg.setTitle(title);
			feedMsg.setLink(link);
			feedMsg.setPubDate(pubDate);
			feedMsg.setType(type);
			feedMsg.setMediaText(mediaText);
			feedMsg.setImgPath(imagePath);
		}

		LOG.info("Method End: getNewsDetails(), Class: PushNotificationHelper");
		return feedMsg;
	}

	/**
	 * Method to send notification to IOS
	 * 
	 * @param deviceId
	 * @param data
	 * @param configList
	 * @return
	 * @throws PushNotificationException
	 * @throws IOException
	 */
	public static String sendIOSNotification(List<DeviceId> deviceIdList, NotificationDetails data, List<Configuration> configList)
			throws PushNotificationException, IOException {

		LOG.info("Method Start: sendIOSNotification(), Class: PushNotificationHelper");
		String apnsHost = null;
		// Integer apnsPort = null;
		String apnsCertPath = null;
		String apnsCertPasswd = null;
		Integer flag = 0;
		String iPhoneAlertMsg = null;
		// String xmlData = null;
		String jsonData = null;

		for (Configuration config : configList) {
			if (Constants.TEXT_APNS_HOST.equalsIgnoreCase(config.getConfigurationType())) {
				apnsHost = config.getScreenContent();
				flag++;
				continue;
			}

			if (Constants.TEXT_APNS_CERT_PATH.equalsIgnoreCase(config.getConfigurationType())) {
				apnsCertPath = config.getScreenContent();
				flag++;
				continue;
			}

			if (Constants.TEXT_APNS_CERT_PASSWORD.equalsIgnoreCase(config.getConfigurationType())) {
				apnsCertPasswd = config.getScreenContent();
				flag++;
				continue;
			}

			if (flag == 3) {
				break;
			}
		}

		try {

			ApnsService service = null;
			String payload = null;
			String certFullPath = null;
			Data objData = null;
			File file = null;

			Gson gson = new GsonBuilder().disableHtmlEscaping().create();

			if (null != deviceIdList && !deviceIdList.isEmpty() && null != deviceIdList.get(0) && null != deviceIdList.get(0).getHcName()) {

				objData = getNotificationData(data, deviceIdList.get(0).getHcName(), Constants.PLATFORM_IOS);

				certFullPath = apnsCertPath + deviceIdList.get(0).getHcName() + ".p12";
				file = new File(certFullPath);

				LOG.info("Certificate Path : " + certFullPath);
				LOG.info("apnsCertPasswd Path : " + apnsCertPasswd);

				iPhoneAlertMsg = objData.getNotiMgs();
				objData.setNotiMgs(null);

				jsonData = gson.toJson(objData);

				LOG.info(jsonData);

				if (null != objData && file.exists()) {

					for (DeviceId device : deviceIdList) {

						try {
							if ("gateway.sandbox.push.apple.com".equals(apnsHost)) {
								service = APNS.newService().withCert(certFullPath, apnsCertPasswd).withSandboxDestination().build();
							} else {
								service = APNS.newService().withCert(certFullPath, apnsCertPasswd).withProductionDestination().build();
							}
						} catch (Exception e) {
							LOG.error("Exception occured :" + e.getMessage());
						}

						try {

							/**
							 * deprecate
							 * 
							 * PayLoad Format changed for iOS10. implemented our
							 * own PayLoad Object to Store Information
							 */

							payload = new PayLoadGenerator(device.getBadgeCount(), jsonData, iPhoneAlertMsg).getPayload();

							LOG.info("PayLoad Information : " + payload);
							LOG.info("IOS Device ID : " + device.getDeviceId());
							LOG.info("IOS Message Format" + jsonData);
							ApnsNotification apnsResult = service.push(device.getDeviceId(), payload);
							LOG.info("Result Expire : " + apnsResult.getExpiry());
							LOG.info("Result  Identifier : " + apnsResult.getIdentifier());
							LOG.info("Result  Payload : " + apnsResult.getPayload());
							LOG.info("iOS notifications sent successfully : " + device.getDeviceId());

						} catch (Exception e) {
							LOG.error("Error occured in iOS push for Token ID: " + device.getDeviceId());
							LOG.error("Error occurred : " + e.getMessage());
							continue;
						}
					}
				} else {
					LOG.warn("Unable to generate Message Body or file not exist!");
				}
			} else {
				LOG.warn(" Device List or HubCiti Name Missing.");
			}

		} catch (Exception e) {
			LOG.error("Error occured in iOS pushes" + e.getMessage());
			throw new PushNotificationException();
		}
		LOG.info("Method End: sendIOSNotification(), Class: PushNotificationHelper");
		return null;
	}

	/**
	 * Method to send notification for Android.
	 * 
	 * @param deviceId
	 * @param data
	 * @param configList
	 * @return
	 * @throws PushNotificationException
	 * @throws IOException
	 */
	public static String sendAndroidNotification(List<DeviceId> deviceIdList, NotificationDetails data,

	List<Configuration> configList) throws PushNotificationException, IOException {
		LOG.info("Method Start: sendAndroidNotification(), Class: PushNotificationHelper");
		String fcmApiKey = null;
		String fcm_url = null;
		Data objData = null;

		for (Configuration config : configList) {
			if (Constants.TEXT_FCM_API_KEY.equalsIgnoreCase(config.getConfigurationType())) {
				fcmApiKey = config.getScreenContent();

			}
			if (Constants.TEXT_FCM_URL.equalsIgnoreCase(config.getConfigurationType())) {
				fcm_url = config.getScreenContent();

			}
		}

		/*
		 * Constructing JSON data.
		 */
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		String gsonData = null;

		// Fetching & Storing HubCiti Information

		if (null != deviceIdList && !deviceIdList.isEmpty() && null != deviceIdList.get(0) && null != deviceIdList.get(0).getHcName()) {

			objData = getNotificationData(data, deviceIdList.get(0).getHcName(), Constants.PLATFORM_ANDROID);
			gsonData = gson.toJson(objData);

			if (null != gsonData) {
				for (DeviceId id : deviceIdList) {
					try {

						sendFCMNotification(id.getDeviceId(), fcmApiKey, fcm_url, gsonData, objData.getNotiMgs());
						LOG.info("ID: " + id.getDeviceId());
						LOG.info("Server Key: " + fcmApiKey);
						LOG.info("JSON Message : " + gsonData);
						LOG.info("Title : " + objData.getNotiMgs());
						LOG.info("Android notifications sent sussessfully");
					} catch (JSONException e) {
						LOG.error("Error occured in sending Android notification for Token ID: " + id.getDeviceId());
						continue;
					} catch (PushNotificationException e) {
						LOG.error("Error occured in sending Android notification for Token ID: " + id.getDeviceId());
						continue;
					}
				}
			} else {
				LOG.error(" Data generated from Json is empty!");
			}
		} else {
			LOG.error(" Device List or HubCiti name not exist!");
		}
		LOG.info("Method End: sendAndroidNotification(), Class: PushNotificationHelper");
		return Constants.SUCCESS_MESSAGE;
	}

	/**
	 * Method to Parse RSS Feeds.
	 * 
	 * @param url
	 * @return RSSFeed object.
	 */
	public static RSSFeed parseRSSFeedType2(String url) {
		LOG.info("Method Start: parseRSSFeedType2(), Class: PushNotificationHelper");
		RSSFeed feed = new RSSFeed(null, null, null);

		try {
			URL objURL = new URL(url);
			DocumentBuilderFactory objDBF = DocumentBuilderFactory.newInstance();
			objDBF.setCoalescing(true);
			DocumentBuilder objDB = objDBF.newDocumentBuilder();
			Document objDoc = objDB.parse(new InputSource(objURL.openStream()));
			objDoc.getDocumentElement().normalize();
			NodeList nodeList = objDoc.getElementsByTagName("item");

			for (int i = 0; i < nodeList.getLength(); i++) {
				String tit = null;
				String longDesc = null;
				String image = null;
				String link = null;
				String date = null;
				Node node = nodeList.item(i);
				Element fstElmnt = (Element) node;

				if (fstElmnt.getElementsByTagName("enclosure") != null) {
					NodeList media = fstElmnt.getElementsByTagName("enclosure");
					if (media.item(0) != null && media.item(0).getAttributes() != null && media.item(0).getAttributes().getNamedItem("url") != null) {
						String mediaurl = media.item(0).getAttributes().getNamedItem("url").getNodeValue();
						image = mediaurl;
					}
				} else {
					image = null;
				}

				NodeList linkList = fstElmnt.getElementsByTagName("link");
				Element linkElement = (Element) linkList.item(0);
				linkList = linkElement.getChildNodes();
				link = ((Node) linkList.item(0)).getNodeValue();
				NodeList titleList = fstElmnt.getElementsByTagName("title");
				Element titleElement = (Element) titleList.item(0);
				titleList = titleElement.getChildNodes();
				tit = ((Node) titleList.item(0)).getNodeValue().replaceAll("([^\\w\\s\'.,-:&\"<>])", "").replaceAll("&ldquo", "'").replaceAll("&rdquo", "'")
						.replaceAll("&rsquo", "'");
				NodeList dateList = fstElmnt.getElementsByTagName("pubDate");
				Element dateElement = (Element) dateList.item(0);
				dateList = dateElement.getChildNodes();
				String datee = ((Node) dateList.item(0)).getNodeValue();
				date = datee.toString();

				NodeList websiteList = fstElmnt.getElementsByTagName("description");
				if (null != websiteList && null != websiteList.item(0)) {
					Element websiteElement = (Element) websiteList.item(0);
					websiteList = websiteElement.getChildNodes();
					if (websiteList.item(0).toString() != null) {
						longDesc = websiteList.item(0).toString();
						if (null != longDesc && !"".equals(longDesc)) {
							longDesc = longDesc.replace("[#text:", "");
							longDesc = longDesc.substring(0, longDesc.length() - 1).trim().replaceAll("([^\\w\\s\'.,-:&\"<>])", "").replaceAll("&ldquo", "'")
									.replaceAll("&rdquo", "'").replaceAll("&rsquo", "'").replace("&39", "'").replaceAll("&mdash", "-");
						}
					}
				}
					
				RSSFeedMessage rssFeedMsg = new RSSFeedMessage();
				rssFeedMsg.setTitle(tit);
				rssFeedMsg.setImgPath(image);
				rssFeedMsg.setLink(link);
				rssFeedMsg.setPubDate(date);
				rssFeedMsg.setMediaText(longDesc);
				feed.getRssFeedMsg().add(rssFeedMsg);

			}
		} catch (MalformedURLException me) {
			LOG.error("Error occurred while connecting to resource" + "  URL: " + url + " ExCeption : " + me.getMessage());

		} catch (ParserConfigurationException pce) {
			LOG.error("Error occurred while connecting to Parsing" + "  URL: " + url + " ExCeption : " + pce.getMessage());
		} catch (SAXException saxe) {
			LOG.error("Error occurred while connecting to Parsing" + "  URL: " + url + " ExCeption : " + saxe.getMessage());
		} catch (IOException mioe) {
			LOG.error("Error occurred while connecting to reading resource : " + "  URL: " + url + " ExCeption : " + mioe.getMessage());
		}

		LOG.info("Method End: parseRSSFeed(), Class: PushNotificationHelper");
		return feed;
	}

	/**
	 * Method to send notification to android using gcm-server.jar.
	 * 
	 * @param jsonData
	 * @param gcmApiKey
	 * @param regIds
	 * @throws PushNotificationException
	 * @throws IOException
	 */
	public static void sendAndroidNotification(String jsonData, String gcmApiKey, DeviceId regId) throws PushNotificationException, IOException {
		LOG.info("Method Start: sendAndroidNotification(), Class: PushNotificationHelper");
		try {
			Sender sender = new Sender(gcmApiKey);
			Message message = new Message.Builder().addData("data", jsonData).collapseKey(Constants.COLLAPSE_KEY + " for " + regId.getHcName()).build();
			try {
				Result result = sender.sendNoRetry(message, regId.getDeviceId());
				LOG.info("Notification Response : " + result.toString());
				LOG.info("Android Device ID " + regId.getDeviceId());
				LOG.info("Android Message " + jsonData);
			} catch (IOException e) {
				LOG.error("Error occurred while sending Notification to Android" + e.getMessage());
			}
		} catch (Exception e) {
			LOG.error("Error occurred in sendAndroidNotification :" + e.getMessage());
			throw new PushNotificationException(e.getMessage());
		}
		LOG.info("Method End: sendAndroidNotification(), Class: PushNotificationHelper");
	}

	/**
	 * Method to build Notification Alert text.
	 * 
	 * @param rssMsgCount
	 * @param isDealOfDay
	 * @return
	 */
	public static String getNotificationAlertMessage(Integer rssMsgCount, Boolean isDealOfDay) {
		LOG.info("Method Start: getNotificationAlertMessage(), Class: PushNotificationHelper");
		String notiMsg = "";

		if (null == rssMsgCount) {
			rssMsgCount = 0;
		}

		if (null == isDealOfDay) {
			isDealOfDay = false;
		}

		if (rssMsgCount != 0) {
			if (rssMsgCount == 1) {
				notiMsg = "Here is the top Breaking News";
			} else {
				notiMsg = "Here are " + rssMsgCount + " top Breaking News";
			}

		}
		LOG.info("Method End: getNotificationAlertMessage(), Class: PushNotificationHelper");
		return notiMsg;
	}

	/**
	 * Method to get Notification data to sent to device.
	 * 
	 * @param notiData
	 * @param hcName
	 * @param platform
	 * @return
	 */
	public static Data getNotificationData(NotificationDetails notiData, String hcName, String platform) {
		// LOG.info("Method Start: getNotificationData(), Class: PushNotificationHelper");
		Integer rssMsgCount = 0;
		Boolean dealFlag = false;
		Map<String, RSSFeedMessage> rssMap = null;
		Data data = new Data();
		List<String> strNewsType = null;

		if (null != notiData.getData().getRssFeedList() && !notiData.getData().getRssFeedList().isEmpty()) {
			strNewsType = new ArrayList<String>();
			rssMap = new HashMap<String, RSSFeedMessage>();
			rssMsgCount = notiData.getData().getRssFeedList().size();
			RSSFeedMessage objMsgData = null;
			for (RSSFeedMessage msg : notiData.getData().getRssFeedList()) {
				objMsgData = new RSSFeedMessage();
				if (Constants.PLATFORM_IOS.equalsIgnoreCase(platform)) {
					objMsgData.setTitle(msg.getType());
					objMsgData.setLink(msg.getLink());
				} else {
					objMsgData.setTitle(msg.getType());
					objMsgData.setLink(msg.getLink());
				}

				String key = msg.getType();
				if (!rssMap.containsKey(key)) {
					rssMap.put(msg.getType(), objMsgData);
					strNewsType.add(key);
				}
			}

			List<RSSFeedMessage> newsTypeList = new ArrayList<RSSFeedMessage>();
			for (String s : strNewsType) {
				newsTypeList.add(rssMap.get(s));
			}
			data.setRssFeedList(newsTypeList);
		}

		if (rssMsgCount != 0 || dealFlag == true) {
			data.setNotiMgs(getNotificationAlertMessage(rssMsgCount, dealFlag));
		} else {
			data = null;
		}
		LOG.info("Method End: getNotificationData(), Class: PushNotificationHelper");
		return data;
	}

	/*
	 * 
	 * Method to send push notification to Android FireBased Cloud messaging
	 * Server.
	 */
	private static void sendFCMNotification(String tokenId, String server_key, String fcm_url, String message, String notMessage)
			throws PushNotificationException, IOException, JSONException {

		URL url = new URL(fcm_url);

		HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		conn.setUseCaches(false);
		conn.setDoInput(true);
		conn.setDoOutput(true);

		conn.setRequestMethod("POST");
		conn.setRequestProperty("Authorization", "key=" + server_key);
		conn.setRequestProperty("Content-Type", "application/json");

		JSONObject notificationMessage = new JSONObject();
		notificationMessage.put("body", notMessage);
		notificationMessage.put("tag", 1);

		JSONObject dataMessage = new JSONObject();
		dataMessage.put("body", message);

		JSONObject jsonRequest = new JSONObject();
		jsonRequest.put("to", tokenId.trim());
		jsonRequest.put("notification", notificationMessage);
		jsonRequest.put("data", dataMessage);

		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		wr.write(jsonRequest.toString());
		wr.flush();
		int status = 0;
		if (null != conn) {
			status = conn.getResponseCode();
		}
		if (status != 0) {

			if (status == 200) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				LOG.info("Android Notification Response : " + reader.readLine());
			} else if (status == 401) {
				LOG.info("Notification Response :401 Error TokenId : " + tokenId);
			} else if (status == 501) {
				LOG.info("Notification Response :501 Error TokenId : " + tokenId);
			} else if (status == 503) {
				LOG.info("Notification Response :503 Error FCM Service is Unavailable    TokenId : " + tokenId);
			}

		}
	}
}
