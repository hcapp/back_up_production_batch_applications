package com.rssfeed.exception;


public class BatchProcessingCategoriesException extends Exception {

	/**
	 * generated serial version id.
	 */
	private static final long serialVersionUID = -1385703071543005434L;

	/**
	 * PushNotificationException default constructor.
	 */
	public BatchProcessingCategoriesException() {
		super();
	}

	/**
	 * 
	 * 
	 * @param message
	 *            as parameter
	 */
	public BatchProcessingCategoriesException(String message) {
		super(message);
	}

	/**
	 * 
	 * 
	 * @param cause
	 *            as parameter
	 */
	public BatchProcessingCategoriesException(Throwable cause) {
		super(cause);
	}

	/**
	 * 
	 * @param message
	 *            as parameter
	 * @param cause
	 *            as parameter
	 */
	public BatchProcessingCategoriesException(String message, Throwable cause) {
		super(message, cause);
	}
}
