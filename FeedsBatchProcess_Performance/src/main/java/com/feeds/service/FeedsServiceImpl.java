package com.feeds.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.feeds.common.ApplicationConstants;
import com.feeds.common.CommonConstants;
import com.feeds.common.EmailComponent;
import com.feeds.common.PropertiesReader;
import com.feeds.common.Utility;
import com.feeds.dao.FeedsDao;
import com.feeds.exception.RssFeedBatchProcessException;
import com.feeds.pojo.Item;
import com.feeds.thread.RSSBatchThread;

public class FeedsServiceImpl implements FeedsService {

	/**
	 * Logger instance.
	 */
	private static Logger LOG = LoggerFactory.getLogger(FeedsServiceImpl.class.getName());

	private FeedsDao feedsDao;

	public void setFeedsDao(FeedsDao feedsDao) {
		this.feedsDao = feedsDao;
	}

	public void processDeletionOperation() throws RssFeedBatchProcessException {
		String strMethodName = "processDeletionOperation";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		try {

			feedsDao.deleteFeedData();

		} catch (RssFeedBatchProcessException e) {
			LOG.error("Inside RssFeedServiceImpl : processDeletionOperation : " + e.getMessage());
			throw new RssFeedBatchProcessException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);

	}

	public void getHubCitiId() throws RssFeedBatchProcessException {
		String strMethodName = "getHubCitiId";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		ArrayList<Item> hubList = null;
		List<RSSBatchThread> rssThreads = new ArrayList<RSSBatchThread>();
		
		try {

			hubList = feedsDao.getHubCitiId();

			for (int i = 0; i < hubList.size(); i++) {

				if (PropertiesReader.getPropertyValue("tyler_hubcitiName").equals(hubList.get(i).getHubCitiName())) {

					LOG.info("START OF THE FEEDS BATCH PROCESS :->" + Calendar.getInstance().getTime());
					LOG.info("========================================================================================");
					LOG.info("TYLER :START OF THE FEEDS BATCH PROCESS :->" + Calendar.getInstance().getTime());

					String[] strNew = { "sports", "health", "business", "top", "breaking", "life", "opinion", "entertainment", "food", "all", "weather", "videos", "photos", "Classifields" };

					for (String category : strNew) {
						LOG.info("Inside Tests :" + category);
						rssThreads.add(new RSSBatchThread(category, hubList.get(i).getHcHubCitiID(),1));
					}
					
					if( !rssThreads.isEmpty()){
						for( RSSBatchThread rssThread :rssThreads){
							try {
								rssThread.getThread().join();
						
							} catch (InterruptedException e) {
								LOG.error("Inside FeedsServiceImpl : getHubCitiId() : " + e.getMessage());
							}
						}
						
					}

					/*
					 * LOG.info("Inside Tests : health");
					 * Utility.getTylerList("health", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : life");
					 * Utility.getTylerList("life", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : top");
					 * Utility.getTylerList("top",
					 * hubList.get(i).getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : opinion");
					 * Utility.getTylerList("opinion", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : business");
					 * Utility.getTylerList("business", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : videos");
					 * Utility.getTylerList("videos", hubList.get(i)
					 * .getHcHubCitiID());
					 */

					/*
					 * LOG.info("Inside Tests : employment");
					 * Utility.getLatestClassifieldFromFtp("Classifields",
					 * hubList .get(i).getHcHubCitiID());
					 */

					/*
					 * LOG.info("Inside Tests : entertainment");
					 * Utility.getTylerList("entertainment", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : food");
					 * Utility.getTylerList("food", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : weather");
					 * Utility.getTylerList("weather", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : allarticles");
					 * Utility.getTylerList("all",
					 * hubList.get(i).getHcHubCitiID());
					 */

					LOG.info("TYLER :END OF THE FEEDS BATCH PROCESS :->" + Calendar.getInstance().getTime());
				} else if (PropertiesReader.getPropertyValue("rockwall_hubcitiName").equals(hubList.get(i).getHubCitiName())) {
					LOG.info("========================================================================================");

					LOG.info("ROCKWALL :START OF THE FEEDS BATCH PROCESS :->" + Calendar.getInstance().getTime());

					String[] strNews = { "good times", "good cause", "good living", "good culture", "good people", "good pets", "good faith", "good thinking",
							"good business", "good sports", "good neighbours", "columnists" };

					
					for (String category : strNews) {
						LOG.info("Inside Tests :" + category);
						rssThreads.add(new RSSBatchThread(category, hubList.get(i).getHcHubCitiID(),2));
					}
					
					if( !rssThreads.isEmpty()){
						for( RSSBatchThread rssThread :rssThreads){
							try {
								rssThread.getThread().join();
						
							} catch (InterruptedException e) {
								LOG.error("Inside FeedsServiceImpl : getHubCitiId() : " + e.getMessage());
							}
						}
						
					}

					/*
					 * LOG.info("Inside Tests : good events/times");
					 * Utility.getRockwallList("good times", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : good cause");
					 * Utility.getRockwallList("good cause", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : good living");
					 * Utility.getRockwallList("good living", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : good business");
					 * Utility.getRockwallList("good business", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : good people");
					 * Utility.getRockwallList("good people", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : good pets");
					 * Utility.getRockwallList("good pets", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : good faith");
					 * Utility.getRockwallList("good faith", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : good thinking");
					 * Utility.getRockwallList("good thinking", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : good health");
					 * Utility.getRockwallList("good health", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : good culture");
					 * Utility.getRockwallList("good culture", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : good sports");
					 * Utility.getRockwallList("good sports", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : columnists");
					 * Utility.getRockwallList("columnists", hubList.get(i)
					 * .getHcHubCitiID());
					 */

					LOG.info("ROCKWALL :END OF THE FEEDS BATCH PROCESS :->" + Calendar.getInstance().getTime());

				} else if (PropertiesReader.getPropertyValue("austin_hubcitiName").equals(hubList.get(i).getHubCitiName())) {
					LOG.info("AUSTIN :START OF THE FEEDS BATCH PROCESS :->" + Calendar.getInstance().getTime());

					String[] strNews = { "citywide", "imagineaustin", "abia", "animalservices", "medicalservices", "fire", "humanservices", "sustainability",
							"parks", "police" };

					for (String category : strNews) {
						LOG.info("Inside Tests :" + category);
						rssThreads.add(new RSSBatchThread(category, hubList.get(i).getHcHubCitiID(),3));
					}
					
					if( !rssThreads.isEmpty()){
						for( RSSBatchThread rssThread :rssThreads){
							try {
 								rssThread.getThread().join();
						
							} catch (InterruptedException e) {
								LOG.error("Inside FeedsServiceImpl : getHubCitiId() : " + e.getMessage());
							}
						}
						
					}
					LOG.info("AUSTIN :END OF THE FEEDS BATCH PROCESS :->" + Calendar.getInstance().getTime());
					/*
					 * LOG.info("Inside Tests : imagineaustin");
					 * Utility.getAustinList("imagineaustin", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : abia");
					 * Utility.getAustinList("abia", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : animalservices");
					 * Utility.getAustinList("animalservices", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : medicalservices");
					 * Utility.getAustinList("medicalservices", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : fire");
					 * Utility.getAustinList("fire", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : humanservices");
					 * Utility.getAustinList("humanservices", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : sustainability");
					 * Utility.getAustinList("sustainability", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : parks");
					 * Utility.getAustinList("parks", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : police");
					 * Utility.getAustinList("police", hubList.get(i)
					 * .getHcHubCitiID());
					 */

				} else if (CommonConstants.KILLEENHUBCITINAME.equals(hubList.get(i).getHubCitiName())) {
					LOG.info("KILLEEN :START OF THE FEEDS BATCH PROCESS :->" + Calendar.getInstance().getTime());

					String[] strNews = { "topstories", "Weather", "Business", "Traffic", "Crime", "Opinion", "obits", "worldnews" };

					
					for (String category : strNews) {
						LOG.info("Inside Tests :" + category);
						rssThreads.add(new RSSBatchThread(category, hubList.get(i).getHcHubCitiID(),4));
					}
					
					if( !rssThreads.isEmpty()){
						for( RSSBatchThread rssThread :rssThreads){
							try {
								rssThread.getThread().join();
						
							} catch (InterruptedException e) {
								LOG.error("Inside FeedsServiceImpl : getHubCitiId() : " + e.getMessage());
							}
						}
						
					}

					/*
					 * LOG.info("Inside Tests : weather");
					 * Utility.getKilleenList("Weather", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : business");
					 * Utility.getKilleenList("Business", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : traffic");
					 * Utility.getKilleenList("Traffic", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : crime");
					 * Utility.getKilleenList("Crime", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : opinion");
					 * Utility.getKilleenList("Opinion", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : obits");
					 * Utility.getKilleenList("obits", hubList.get(i)
					 * .getHcHubCitiID());
					 * 
					 * LOG.info("Inside Tests : worldnews");
					 * Utility.getKilleenList("worldnews", hubList.get(i)
					 * .getHcHubCitiID());
					 */
					LOG.info("KILLEEN :END OF THE FEEDS BATCH PROCESS :->" + Calendar.getInstance().getTime());
				}

			}

		} catch (RssFeedBatchProcessException e) {
			LOG.error("Inside FeedServiceImpl : getHubCitiId : " + e.getMessage());
			throw new RssFeedBatchProcessException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);

	}

	public String processDatabaseOperation(List<Item> items, String name, String hubcitiId) throws RssFeedBatchProcessException {
		String strMethodName = "processDatabaseOperation";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);

		String response = null;
		String portingResponse = null;

		try {

			if (null != items && !items.isEmpty()) {

				response = feedsDao.insertData(items, name, hubcitiId);

				if (null != response && response.equals(ApplicationConstants.SUCCESSTEXT)) {
					LOG.info("Inside FeedsBatchProcssServiceImpl : processDatabaseOperation :  " + ApplicationConstants.SUCCESSTEXT);
				} else {
					LOG.info("Inside FeedsBatchProcssServiceImpl : processDatabaseOperation :  " + ApplicationConstants.FAILURETEXT);
					portingResponse = ApplicationConstants.FAILURETEXT;
				}

			} else {
				response = "No Items to insert";
				LOG.info("Inside FeedsBatchProcssServiceImpl : processDatabaseOperation : " + " No Items in the List to insert into database.");
			}
		} catch (RssFeedBatchProcessException e) {
			LOG.error("Inside FeedsBatchProcssServiceImpl : processDatabaseOperation : " + e.getMessage());
			throw new RssFeedBatchProcessException(e);
		}

		LOG.info(ApplicationConstants.METHODEND + strMethodName);
		return portingResponse;
	}

	public String NewsFeedPorting() throws RssFeedBatchProcessException {
		String strPortingResponse = null;
		String strMethodName = "NewsFeedPorting";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		try {

			strPortingResponse = feedsDao.NewsFeedPorting();

			if (null != strPortingResponse && strPortingResponse.equals(ApplicationConstants.SUCCESSTEXT)) {
				LOG.info("Inside FeedsBatchProcssServiceImpl : NewsFeedPorting :  " + ApplicationConstants.SUCCESSTEXT);
			} else {
				LOG.info("Inside FeedsBatchProcssServiceImpl : NewsFeedPorting :  " + ApplicationConstants.FAILURETEXT);
			}
			LOG.info(ApplicationConstants.METHODEND + strMethodName);
		} catch (RssFeedBatchProcessException exception) {
			LOG.error("Inside FeedsBatchProcssServiceImpl : processDatabaseOperation : " + exception.getMessage());
			throw new RssFeedBatchProcessException(exception);
		}

		return strPortingResponse;
	}

	public void deleteData() throws RssFeedBatchProcessException {
		String strMethodName = "deleteData";
		LOG.info(ApplicationConstants.METHODSTART + strMethodName);
		try {

			feedsDao.deleteFeedData();

		} catch (RssFeedBatchProcessException exception) {
			LOG.error("Inside FeedsBatchProcssServiceImpl : processDatabaseOperation : " + exception.getMessage());
			throw new RssFeedBatchProcessException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + strMethodName);
	}

	public String emptyNewsListByEmail() {
		LOG.info("Inside RssFeedServiceImpl : emptyNewsListByEmail");

		String response = null;
		List<Item> newsItemList = null;

		final String strSmtpPort = PropertiesReader.getPropertyValue(ApplicationConstants.SMTP_PORT);
		final String strSmtpHost = PropertiesReader.getPropertyValue(ApplicationConstants.SMTP_SERVER);
		final String strSubject = PropertiesReader.getPropertyValue(ApplicationConstants.EMAIL_SUBJECT);
		final String strEmailrecipient = PropertiesReader.getPropertyValue(ApplicationConstants.SENDER_TO_LIST);
		final String strFromEmailId = PropertiesReader.getPropertyValue(ApplicationConstants.FROM_MAIL);
		final String strEmailrecipients[] = strEmailrecipient.split(",");

		try {
			newsItemList = feedsDao.emptyNewsListByEmail();

			if (null != newsItemList && !newsItemList.isEmpty()) {
				final String strMailContent = Utility.emailBody(newsItemList);
				response = ApplicationConstants.SUCCESSTEXT;
				EmailComponent.multipleUsersmailingComponent(strFromEmailId, strEmailrecipients, strSubject, strMailContent, strSmtpHost, strSmtpPort);
			} else {
				response = ApplicationConstants.FAILURETEXT;
			}

		} catch (MessagingException e) {
			LOG.error("Inside RssFeedServiceImpl : emptyNewsListByEmail :  MessagingException : " + e.getMessage());

		} catch (RssFeedBatchProcessException e) {
			LOG.error("Inside RssFeedServiceImpl : emptyNewsListByEmail : " + e.getMessage());
		}

		LOG.info("Exit RssFeedServiceImpl : emptyNewsListByEmail : " + response);
		return response;
	}

}
