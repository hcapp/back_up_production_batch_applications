package com.scansee.batch.common;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.scansee.batch.common.pojo.BatchProcessStatus;
import com.scansee.batch.common.pojo.HotDealDetail;
import com.scansee.batch.exception.PlumDistrictBatchProcessException;

/**
 * @author sowjanya_d
 */
public class Utility
{
	private static final Logger LOG = Logger.getLogger(Utility.class);

	/**
	 * @param connection
	 * @param statement
	 * @param resultSet
	 */
	public static void closeResources(Connection connection, Statement statement, ResultSet resultSet)
	{
		try
		{
			if (null != connection)
			{
				connection.close();
			}
			if (null != statement)
			{
				statement.close();
			}
			if (null != resultSet)
			{
				resultSet.close();
			}
		}
		catch (SQLException e)
		{
			LOG.info(" Problem while closing  the resources.", e);
		}

	}

	/**
	 * @param stringBuilder
	 * @param list
	 */
	public static StringBuilder iterateList(StringBuilder stringBuilder, ArrayList<Integer> list)
	{
		int count = list.size();
		for (Integer i : list)
		{
			count--;
			stringBuilder.append(i);
			if (0 == count)
			{
				stringBuilder.append(".");
			}
			else
			{
				stringBuilder.append(",");
			}

		}

		return stringBuilder;
	}

	/**
	 * @param firstList
	 * @param secondList
	 * @return
	 */
	public static ArrayList<Integer> removeDuplicates(ArrayList<Integer> firstList, ArrayList<Integer> secondList)
	{
		ArrayList<Integer> temp = new ArrayList<Integer>();

		for (Integer i : firstList)
		{
			if (secondList.contains(i))
			{
				temp.add(i);
			}
		}

		firstList.removeAll(temp);

		return firstList;
	}

	public static String formResponseXml(String errorCode, String errorResponse)
	{
		LOG.info("In formResponseXml method.");
		final StringBuilder response = new StringBuilder();
		response.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		response.append("<response>");
		response.append("<responseCode>");
		response.append(errorCode);
		response.append("</responseCode>");
		response.append("<responseText>");
		response.append(errorResponse);
		response.append("</responseText>");
		response.append("</response>");
		return response.toString();
	}

	/**
	 * The method to get the current date formated in yyyy-MM-dd HH:mm:ss
	 * format.
	 * 
	 * @return The current date formatted in yyyy-MM-dd HH:mm:ss forma
	 */

	public static String getFormattedCurrentDate()
	{
		final String methodName = "getFormattedDate";
		LOG.info(Constants.METHODSTART + methodName);

		final Date date = new Date();
		/*
		 * formatting the current date.
		 */
		String currentDateStr = null;
		try
		{
			final DateFormat formater = new SimpleDateFormat("MM-dd-yyyy");
			final Date parsedUtilDate = formater.parse(formater.format(date));
			currentDateStr = formater.format(parsedUtilDate);
		}
		catch (ParseException exception)
		{
			LOG.info("Exception in convertDBdate method" + exception.getMessage());
			return Constants.NOTAPPLICABLE;
		}

		LOG.info(Constants.METHODEND + methodName);
		return currentDateStr;
	}

	@SuppressWarnings("rawtypes")
	public static ArrayList<HotDealDetail> getHotDealDetailFromXLSXFile(InputStream inputStream) throws PlumDistrictBatchProcessException
	{

		LOG.info("Inside getHotDealDetailFromXLSXFile method ");
		ArrayList<HotDealDetail> dealDetailList = new ArrayList<HotDealDetail>();
		try
		{

			// XSSFWorkbook is used for reading xlsx file
			XSSFWorkbook wb_xssf = new XSSFWorkbook(inputStream);
			// Get first sheet of Excel file
			XSSFSheet sheet = wb_xssf.getSheetAt(0);
			Iterator rowItr = sheet.rowIterator();
			// Iterate each row
			while (rowItr.hasNext())
			{
				XSSFRow row = (XSSFRow) rowItr.next();
				HotDealDetail dealDetail = new HotDealDetail();
				dealDetail.setHotdealId(((row.getCell(0)) != null) ? row.getCell(0).toString() : null);
				dealDetail.setPartnerName((row.getCell(1) != null) ? row.getCell(1).toString() : null);
				dealDetail.setProvider((row.getCell(2) != null) ? row.getCell(2).toString() : null);
				dealDetail.setPrice((row.getCell(3) != null) ? row.getCell(3).toString() : null);
				dealDetail.setDiscount((row.getCell(4) != null) ? row.getCell(4).toString() : null);
				dealDetail.setSalePrice((row.getCell(5) != null) ? row.getCell(5).toString() : null);
				dealDetail.setHotdealName((row.getCell(6) != null) ? row.getCell(6).toString() : null);
				dealDetail.setShortDesc((row.getCell(7) != null) ? row.getCell(7).toString() : null);
				dealDetail.setLongDesc((row.getCell(8) != null) ? row.getCell(8).toString() : null);
				dealDetail.setImageUrl((row.getCell(9) != null) ? row.getCell(9).toString() : null);
				dealDetail.setTermsAndCondtions((row.getCell(10) != null) ? row.getCell(10).toString() : null);
				dealDetail.setUrl((row.getCell(11) != null) ? row.getCell(11).toString() : null);

				if (row.getRowNum() != 0)
				{
					dealDetail.setStartDate((row.getCell(12) != null && !row.getCell(12).toString().equals("")) ? formattedDate(row.getCell(12)
							.getDateCellValue().toString()) : null);
					dealDetail.setEndDate((row.getCell(13) != null && !row.getCell(13).toString().equals("")) ? formattedDate(row.getCell(13)
							.getDateCellValue().toString()) : null);
				}
				else
				{

					dealDetail.setStartDate((row.getCell(12) != null) ? row.getCell(12).toString() : null);
					dealDetail.setEndDate((row.getCell(13) != null) ? row.getCell(13).toString() : null);
				}

				dealDetail.setCategory((row.getCell(14) != null) ? row.getCell(14).toString() : null);
				dealDetail.setCreatedDate((row.getCell(15) != null) ? row.getCell(15).toString() : null);
				dealDetail.setModifiedDate((row.getCell(16) != null) ? row.getCell(16).toString() : null);
				dealDetail.setCity((row.getCell(17) != null) ? row.getCell(17).toString() : null);
				dealDetail.setState((row.getCell(18) != null) ? row.getCell(18).toString() : null);
				dealDetail.setThirdPartyId((row.getCell(19) != null) ? row.getCell(19).toString() : null);
				dealDetailList.add(dealDetail);
			}

			// Remove header
			if (!dealDetailList.isEmpty())
			{

				dealDetailList.remove(0);
			}

			else
			{

				LOG.error("Invalid File Name");
			}

		}
		catch (FileNotFoundException e)
		{

			LOG.error("Exception occered in getHotDealDetailFromXLSXFile Method\n" + e.getMessage());
			throw new PlumDistrictBatchProcessException(e);
		}
		catch (IOException e)
		{
			LOG.error("Exception occered in getHotDealDetailFromXLSXFile Method\n" + e.getMessage());
			throw new PlumDistrictBatchProcessException(e);
		}
		catch (ParseException e)
		{
			LOG.error("Exception occered in getHotDealDetailFromXLSXFile Method\n" + e.getMessage());
			throw new PlumDistrictBatchProcessException(e);
		}
		LOG.info("Exist getHotDealDetailFromXLSXFile method ");
		return dealDetailList;
	}

	/**
	 * getFormattedDate method will convert date format to MM/dd/yyyy.
	 * 
	 * @param enteredDate
	 *            As input parameter
	 * @return converted date.
	 * @throws java.text.ParseException
	 *             Exception while parsing date.
	 */

	public static String formattedDate(String enteredDate) throws java.text.ParseException
	{
		LOG.info("Inside the method formattedDate");
		String cDate = null;
		if (null != enteredDate && !"".equals(enteredDate))
		{

			// dd-MMM-yyyy
			DateFormat oldFormatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
			DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

			Date convertedDate = (Date) oldFormatter.parse(enteredDate);
			cDate = formatter.format(convertedDate);
		}
		LOG.info("Exit method formattedDate");
		return cDate;
	}

	public static java.sql.Timestamp getFormattedDate() throws java.text.ParseException
	{
		final String methodName = "getFormattedDate";
		// LOG.info(ApplicationConstants.METHODSTART + methodName);

		final Date date = new Date();
		java.sql.Timestamp sqltDate = null;
		/*
		 * formatting the current date.
		 */
		// final DateFormat formater = new SimpleDateFormat("yyyy-dd-MM");
		final DateFormat formater = new SimpleDateFormat("MM-dd-yyyy");
		java.util.Date parsedUtilDate;
		parsedUtilDate = formater.parse(formater.format(date));
		sqltDate = new java.sql.Timestamp(parsedUtilDate.getTime());
		// LOG.info(ApplicationConstants.METHODEND + methodName);
		return sqltDate;
	}

	public static String formEmailBody(ArrayList<BatchProcessStatus> batchProcessStatusList)
	{

		StringBuilder emailBody = new StringBuilder();
		emailBody.append("Hi,");
		emailBody.append("</br></br>");

		if (batchProcessStatusList != null)
		{

			if (batchProcessStatusList.isEmpty())
			{

				emailBody.append("Batch Process logs are not available for the date " + Utility.getCurrentDate() + ".\n");
				emailBody.append("Please Verify Batch processes.");
			}
			else
			{

				emailBody.append("Please Find the status of Batch Processes.");
				emailBody.append("</br></br>");
				emailBody
						.append("<table cellspacing='0' cellpadding='0' border='1'><tr style='background-color: yellow;'><th>Batch Program Name</th><th>Excecution Date</th>");

				emailBody.append("<th># Rows Received</th>");
				emailBody.append("<th># Rows Processed</th>");
				emailBody.append("<th>Duplicates Count</th>");
				emailBody.append("<th>Expired Count</th>");
				emailBody.append("<th>Mandatory Fields Missing Count</th>");
				emailBody.append("<th>Status</th>");
				emailBody.append("<th>Reason</th>");
				emailBody.append("<th>ProcessedFileName</th>");

				emailBody.append("</tr>");
				for (int i = 0; i < batchProcessStatusList.size(); i++)
				{
					BatchProcessStatus batchProcessStatus = batchProcessStatusList.get(i);
					emailBody.append("<tr>");
					emailBody.append("<td>");
					emailBody.append(batchProcessStatus.getBatchProcessName());
					emailBody.append("</td>");
					emailBody.append("<td>");
					emailBody.append(batchProcessStatus.getExecutionDate());
					emailBody.append("</td>");
					emailBody.append("<td>");
					if (null != batchProcessStatus.getProcessedFileName() && !batchProcessStatus.getProcessedFileName().equals(""))
					{
						emailBody.append(batchProcessStatus.getRowsRecieved());
					}
					else
					{

						emailBody.append("0");
					}
					emailBody.append("</td>");
					emailBody.append("<td>");
					if (null != batchProcessStatus.getProcessedFileName() && !batchProcessStatus.getProcessedFileName().equals(""))
					{
						emailBody.append(batchProcessStatus.getRowsProcessed());
					}
					else
					{

						emailBody.append("0");
					}
					emailBody.append("</td>");
					emailBody.append("<td>");

					emailBody.append(batchProcessStatus.getDuplicatesCount());

					emailBody.append("</td>");

					emailBody.append("<td>");

					emailBody.append(batchProcessStatus.getExpiredCount());

					emailBody.append("</td>");

					emailBody.append("<td>");

					emailBody.append(batchProcessStatus.getMandatoryFieldsMissingCount());

					emailBody.append("</td>");
					if (null != batchProcessStatus.getStatus() && batchProcessStatus.getStatus().equals("1"))
					{
						emailBody.append("<td>");
						emailBody.append("Success");
						emailBody.append("</td>");
						emailBody.append("<td>");
						emailBody.append("N/A");
						emailBody.append("</td>");

					}
					else if (null != batchProcessStatus.getStatus() && batchProcessStatus.getStatus().equals("0"))
					{

						emailBody.append("<td>");
						emailBody.append("Failure");
						emailBody.append("</td>");
						emailBody.append("<td>");
						emailBody.append(batchProcessStatus.getReason());
						emailBody.append("</td>");
					}

					emailBody.append("<td>");
					if (null != batchProcessStatus.getProcessedFileName() && !batchProcessStatus.getProcessedFileName().equals(""))
					{

						emailBody.append(batchProcessStatus.getProcessedFileName());
					}
					else
					{

						emailBody.append("N/A");
					}
					emailBody.append("</td>");
					emailBody.append("</tr>");

				}
				emailBody.append("</table>");
			}
		}
		else
		{

			emailBody.append("Batch Process logs are not available for the date " + Utility.getCurrentDate() + ".\n");
			emailBody.append("Please Verify Batch processes.");
		}

		emailBody.append("</br></br>");
		emailBody.append("Regards</br>");
		emailBody.append("ScanSee Team");

		return emailBody.toString();
	}

	public static String getCurrentDate()
	{
		String currentDate = null;
		try
		{
			DateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
			Date date1 = new Date();
			currentDate = df.format(date1);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			// TODO: handle exception
		}
		return currentDate;
	}

	public static Date formatDateNoDelimeter(String date) throws PlumDistrictBatchProcessException
	{
		
		LOG.info("Inside formatDateNoDelimeter:"+ "Recieved Date:"+date);

		Date convertedDate = null;
		try
		{
			if (null != date && !"".equals(date))
			{
				DateFormat formatter = new SimpleDateFormat("MMddyyyy");
				convertedDate = (Date) formatter.parse(date);

			}

		}
		catch (Exception e)
		{
			LOG.error("Exception occurred in formatDateNoDelimeter:" + e);
			throw new PlumDistrictBatchProcessException(e.getMessage());
		}
		LOG.info("Exit formatDateNoDelimeter");
		return convertedDate;
	}

	public static void main(String[] arg) throws PlumDistrictBatchProcessException
	{

		// /formatDateNoDelimeter("23322013");
	}
}
