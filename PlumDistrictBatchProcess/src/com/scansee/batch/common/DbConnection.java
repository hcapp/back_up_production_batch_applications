package com.scansee.batch.common;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.scansee.batch.exception.PlumDistrictBatchProcessException;

/**
 * The class for connecting database.
 * 
 * @author sowjanya_d
 */
public class DbConnection
{

	/**
	 * Instance of Logger.
	 */
	private static final Logger LOG = Logger.getLogger(DbConnection.class);

	/**
	 * Method to DataBase Connection.
	 * 
	 * @return JdbcTemplate
	 * @throws PlumDistrictBatchProcessException
	 *             The exceptions are caught and a PlumDistrictBatchProcessException
	 *             defined for the application is thrown.
	 */
	public JdbcTemplate getConnection() throws PlumDistrictBatchProcessException
	{
		JdbcTemplate jdbcTemplate = null;

		try
		{
			final SingleConnectionDataSource singleConnectionDataSource = new SingleConnectionDataSource();
			singleConnectionDataSource.setDriverClassName(PropertiesReader.getPropertyValue("driver_className"));
			singleConnectionDataSource.setUrl(PropertiesReader.getPropertyValue("db_url"));
			singleConnectionDataSource.setUsername(PropertiesReader.getPropertyValue("db_username"));
			singleConnectionDataSource.setPassword(PropertiesReader.getPropertyValue("db_password"));
			jdbcTemplate = new JdbcTemplate(singleConnectionDataSource);
		}
		catch (DataAccessException e)
		{
			LOG.error("Exception occured in getConnection" + e);
			throw new PlumDistrictBatchProcessException(e);
		}

		return jdbcTemplate;
	}

	/**
	 * Method to DataBase Connection.
	 * 
	 * @return JdbcTemplate
	 * @throws PlumDistrictBatchProcessException
	 *             The exceptions are caught and a PlumDistrictBatchProcessException
	 *             defined for the application is thrown.
	 */
	public SimpleJdbcTemplate getSimpleJdbcTemplate() throws PlumDistrictBatchProcessException
	{
		SimpleJdbcTemplate simpleJdbcTemplate = null;

		try
		{
			final SingleConnectionDataSource singleConnectionDataSource = new SingleConnectionDataSource();
			singleConnectionDataSource.setDriverClassName(PropertiesReader.getPropertyValue("driver_className"));
			singleConnectionDataSource.setUrl(PropertiesReader.getPropertyValue("db_url"));
			singleConnectionDataSource.setUsername(PropertiesReader.getPropertyValue("db_username"));
			singleConnectionDataSource.setPassword(PropertiesReader.getPropertyValue("db_password"));
			simpleJdbcTemplate = new SimpleJdbcTemplate(singleConnectionDataSource);
		}
		catch (DataAccessException e)
		{
			LOG.error("Exception occured in getConnection" + e);
			throw new PlumDistrictBatchProcessException(e);
		}

		return simpleJdbcTemplate;
	}
}
