package com.scansee.batch.common.pojo;

public class HotDealDetail
{
	private String hotdealId;
	private String partnerName;
	private String provider;
	private String price;
	private String discount;
	private String salePrice;
	private String hotdealName;
	private String shortDesc;
	private String longDesc;
	private String imageUrl;
	private String termsAndCondtions;
	private String url;
	private String startDate;
	private String endDate;
	private String category;
	private String createdDate;
	private String modifiedDate;
	private String city;
	private String state;
	private String thirdPartyId;
	public String getHotdealId()
	{
		return hotdealId;
	}
	public void setHotdealId(String hotdealId)
	{
		this.hotdealId = hotdealId;
	}
	public String getPartnerName()
	{
		return partnerName;
	}
	public void setPartnerName(String partnerName)
	{
		this.partnerName = partnerName;
	}
	public String getProvider()
	{
		return provider;
	}
	public void setProvider(String provider)
	{
		this.provider = provider;
	}
	public String getPrice()
	{
		return price;
	}
	public void setPrice(String price)
	{
		this.price = price;
	}
	public String getDiscount()
	{
		return discount;
	}
	public void setDiscount(String discount)
	{
		this.discount = discount;
	}

	public String getSalePrice()
	{
		return salePrice;
	}
	public void setSalePrice(String salePrice)
	{
		this.salePrice = salePrice;
	}
	public String getHotdealName()
	{
		return hotdealName;
	}
	public void setHotdealName(String hotdealName)
	{
		this.hotdealName = hotdealName;
	}
	public String getShortDesc()
	{
		return shortDesc;
	}
	public void setShortDesc(String shortDesc)
	{
		this.shortDesc = shortDesc;
	}
	public String getLongDesc()
	{
		return longDesc;
	}
	public void setLongDesc(String longDesc)
	{
		this.longDesc = longDesc;
	}
	public String getImageUrl()
	{
		return imageUrl;
	}
	public void setImageUrl(String imageUrl)
	{
		this.imageUrl = imageUrl;
	}
	public String getUrl()
	{
		return url;
	}
	public void setUrl(String url)
	{
		this.url = url;
	}
	public String getStartDate()
	{
		return startDate;
	}
	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}
	public String getEndDate()
	{
		return endDate;
	}
	public void setEndDate(String endDate)
	{
		this.endDate = endDate;
	}
	public String getCategory()
	{
		return category;
	}
	public void setCategory(String category)
	{
		this.category = category;
	}
	public String getCreatedDate()
	{
		return createdDate;
	}
	public void setCreatedDate(String createdDate)
	{
		this.createdDate = createdDate;
	}
	public String getModifiedDate()
	{
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}
	public String getCity()
	{
		return city;
	}
	public void setCity(String city)
	{
		this.city = city;
	}
	public String getState()
	{
		return state;
	}
	public void setState(String state)
	{
		this.state = state;
	}
	public String getThirdPartyId()
	{
		return thirdPartyId;
	}
	public void setThirdPartyId(String thirdPartyId)
	{
		this.thirdPartyId = thirdPartyId;
	}
	public String getTermsAndCondtions()
	{
		return termsAndCondtions;
	}
	public void setTermsAndCondtions(String termsAndCondtions)
	{
		this.termsAndCondtions = termsAndCondtions;
	}
	
}
