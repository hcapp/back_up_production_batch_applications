package com.scansee.batch.dao;

import java.util.ArrayList;
import java.util.List;
import com.scansee.batch.exception.NewsTemplateBatchProcessesException;
import com.scansee.batch.pojo.Item;

/**
 * Inserting data to staging table and main table, and send email.
 * 
 * @author vaidehi.ne
 */
public interface NewsTemplateDao {
	/**
	 * The method for inserting NewsTemplate data to staging and main table once
	 * batch processed .
	 * 
	 * @param
	 * @throws NewsTemplateBatchProcessesException
	 */

	String insertData(List<Item> items, String newsType, String subcategory, String hubcitiId) throws NewsTemplateBatchProcessesException;

	public List<Item> emptyNewsListByEmail() throws NewsTemplateBatchProcessesException;

	public String NewsFeedPorting() throws NewsTemplateBatchProcessesException;

	public ArrayList<Item> getHubitiInfo() throws NewsTemplateBatchProcessesException;

	public String deleteFeedData() throws NewsTemplateBatchProcessesException;
}
