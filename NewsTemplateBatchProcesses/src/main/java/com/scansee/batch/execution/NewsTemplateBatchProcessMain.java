package com.scansee.batch.execution;

import java.util.Calendar;
import java.util.logging.Logger;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.scansee.batch.common.ApplicationConstants;
import com.scansee.batch.exception.NewsTemplateBatchProcessesException;

import com.scansee.batch.service.NewsTemplateServiceImpl;

/**
 * Batch Process for News Template.
 * 
 * @author vaidehi.ne
 */
public class NewsTemplateBatchProcessMain {
	private static Logger LOG = Logger.getLogger(NewsTemplateBatchProcessMain.class.getName());

	public static void main(String[] args) throws NewsTemplateBatchProcessesException {

		LOG.info("START OF THE NEWS TEMPLATE BATCH PROCESS :->" + Calendar.getInstance().getTime());

		@SuppressWarnings("resource")
		final ApplicationContext context = new ClassPathXmlApplicationContext("newsTemplate-service.xml");
		final NewsTemplateServiceImpl newsTemplateService = (NewsTemplateServiceImpl) context.getBean("newsTemplateService");
		try

		{
			
			LOG.info("*************************************NewsTemplateBatchProcess Start******************************************");
			LOG.info("Inside Main Method Process Start @:" + Calendar.getInstance().getTime());

			/**
			 * Delete history from staging table
			 */
			newsTemplateService.deleteFeedHistory();
			/**
			 * Get rss feed data from XML Parser
			 */
			newsTemplateService.getNewsFeedDetails();
			LOG.info("NEWS INSERTION COMPLETED IN STAGING TABLE...");

			/**
			 * insert feed data from staging table to main table
			 */
			newsTemplateService.NewsFeedPorting();
			LOG.info("NEWS INSERTION COMPLETED IN MAIN TABLE...");
			/**
			 * sending list of items with empty information email details to
			 * database
			 */
			LOG.info("Inside Tests : mailSending");
			newsTemplateService.emptyNewsListByEmail();

		} catch (NewsTemplateBatchProcessesException e) {
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + e.getMessage());
		}
		LOG.info("Inside Main Method Process Ends  @:" + Calendar.getInstance().getTime());
		LOG.info("*************************************NewsTemplateBatchProcess Ends******************************************");
	}

}
