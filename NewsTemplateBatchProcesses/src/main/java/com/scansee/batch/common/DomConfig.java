package com.scansee.batch.common;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class DomConfig {
	

	public static DocumentBuilder getDocumentConfig() throws Exception{
		
		
		DocumentBuilder db = null;
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setCoalescing(true);
		try {
			db = dbf.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			throw new Exception(e);
		}
		
		return db;
	}
	

}
