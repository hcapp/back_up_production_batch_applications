package com.scansee.batch.service;

import java.util.List;

import com.scansee.batch.exception.NewsTemplateBatchProcessesException;
import com.scansee.batch.pojo.Item;

public interface NewsTemplateService {

	String processDatabaseOperation(List<Item> items, String category, String subcategory, String hubcitiId) throws NewsTemplateBatchProcessesException;

	public String deleteFeedHistory() throws NewsTemplateBatchProcessesException;

	public String NewsFeedPorting() throws NewsTemplateBatchProcessesException;

	public String getNewsFeedDetails() throws NewsTemplateBatchProcessesException;

	public String emptyNewsListByEmail() throws NewsTemplateBatchProcessesException;

}
