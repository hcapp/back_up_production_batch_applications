package com.feeds.service;

import java.util.List;

import com.feeds.exception.RssFeedBatchProcessException;
import com.feeds.pojo.Item;

public interface FeedsService {

	/**
	 * @param items
	 * @param name
	 * @param hubcitiId
	 * @return
	 * @throws RssFeedBatchProcessException
	 */
	String processDatabaseOperation(List<Item> items, String name,
			String hubcitiId) throws RssFeedBatchProcessException;

	/**
	 * @return
	 * @throws RssFeedBatchProcessException
	 */
	void processDeletionOperation() throws RssFeedBatchProcessException;

	/**
	 * @return
	 * @throws RssFeedBatchProcessException
	 */
	String NewsFeedPorting() throws RssFeedBatchProcessException;

	void getHubCitiId() throws RssFeedBatchProcessException;

	/**
	 * @throws RssFeedBatchProcessException
	 */
	void deleteData() throws RssFeedBatchProcessException;

	/**
	 * The DAO method for sending list of NewsFeed Type with empty information
	 * thru email..
	 * 
	 * @return Succes or Failure mail sending.
	 * @throws RssFeedBatchProcessException
	 *             throws if exception occurs.
	 */
	String emptyNewsListByEmail() throws RssFeedBatchProcessException;

}
