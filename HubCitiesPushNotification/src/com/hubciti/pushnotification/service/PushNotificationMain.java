package com.hubciti.pushnotification.service;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hubciti.pushnotification.common.helper.BatchTime;
import com.hubciti.pushnotification.common.helper.PropertiesReader;
import com.hubciti.pushnotification.exception.PushNotificationException;

/*---------------------------------------------------------------
 * PushNotificationMain Initiating push notification process 
 * 
 * @Author : Kirankumar Garaddi
 * @written : 7/12/2015
 * 
 * Compilation : PushNotificationMain.java
 * Execution   : PushNotificationMain
 * 
 ----------------------------------------------------------------*/

public class PushNotificationMain {

	private static final Logger LOG = (Logger) LoggerFactory.getLogger(PushNotificationMain.class);
			
	public static void main(String[] args) {

		LOG.info("-----------------------------------------------------------------------------");
		Calendar startCalendar = Calendar.getInstance();
		LOG.info("HubCiti Push Notification Process Starts @:" + startCalendar.getTime().toString());
		LOG.info("-----------------------------------------------------------------------------");
		PushNotificationService pushNotificationService = new PushNotificationService();
		
		//read HubCities from property file
		String hubCitiesString = PropertiesReader.getPropertyValue("HUBCITIIDS");		
		if( null != hubCitiesString ){			
			//get hubcities in array format
			String[] hubCitiIds = hubCitiesString.split(",");			
			for(String hubCitiId : hubCitiIds){				
				try {					
					Integer id = Integer.parseInt(hubCitiId);					
					if( null != id){
						//send notification to hubCiti
						LOG.info("---------------------------------");	
						LOG.info("Begin Notification to HubCiti " + id);	
						pushNotificationService.sendNotification(id);
						LOG.info("End Notification to HubCiti " + id);	
						LOG.info("---------------------------------");	
					}
				} catch (PushNotificationException e) {
					LOG.error("Error in PushNotificationMain: " + e.getMessage());
				}
			}			
		}
		
		
		LOG.info("*****************************************************************************");
		Calendar endCalendar = Calendar.getInstance();
		LOG.info("HubCiti Push Notification  Process Ends @:" + endCalendar.getTime().toString());
		BatchTime.printTime(startCalendar, endCalendar);
		LOG.info("*****************************************************************************");
		
	}

}

