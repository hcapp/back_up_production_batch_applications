package com.hubciti.pushnotification.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hubciti.pushnotification.common.helper.Constants;
import com.hubciti.pushnotification.common.pojo.Configuration;
import com.hubciti.pushnotification.common.pojo.Data;
import com.hubciti.pushnotification.common.pojo.Deal;
import com.hubciti.pushnotification.common.pojo.DeviceId;
import com.hubciti.pushnotification.common.pojo.NotificationDetails;
import com.hubciti.pushnotification.exception.PushNotificationException;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
/*---------------------------------------------------------------
 * Deals Push Notification 
 * 
 *  @written : 03/07/2016
 * 
 * Compilation : PushNotificationHelper.java
 * Execution   : PushNotificationHelper
 *     
 * 
 ----------------------------------------------------------------*/
public class PushNotificationHelper {

	private static final Logger LOG = LoggerFactory.getLogger(PushNotificationHelper.class);

	/**
	 * Method to send notification to IOS
	 * 
	 * @param deviceId
	 * @param data
	 * @param configList
	 * @return
	 * @throws PushNotificationException
	 * @throws IOException
	 */
	public static String sendIOSNotification(List<DeviceId> deviceIdList, NotificationDetails data, List<Configuration> configList)
			throws PushNotificationException, IOException {
		LOG.info("Method Start: sendIOSNotification(), Class: PushNotificationHelper");
		String apnsHost = null;
		// Integer apnsPort = null;
		String apnsCertPath = null;
		String apnsCertPasswd = null;
		Integer flag = 0;
		String iPhoneAlertMsg = null;
		// String xmlData = null;
		String jsonData = null;

		for (Configuration config : configList) {
			if (Constants.TEXT_APNS_HOST.equalsIgnoreCase(config.getConfigurationType())) {
				apnsHost = config.getScreenContent();
				flag++;
				continue;
			}

			/*
			 * 
			 * if(Constants.TEXT_APNS_PORT.equalsIgnoreCase(config. getConfigurationType())) { apnsPort = Integer.parseInt(config.getScreenContent());
			 * flag++; continue; }
			 */

			if (Constants.TEXT_APNS_CERT_PATH.equalsIgnoreCase(config.getConfigurationType())) {
				apnsCertPath = config.getScreenContent();
				flag++;
				continue;
			}

			if (Constants.TEXT_APNS_CERT_PASSWORD.equalsIgnoreCase(config.getConfigurationType())) {
				apnsCertPasswd = config.getScreenContent();
				flag++;
				continue;
			}

			if (flag == 3) {
				break;
			}
		}

		try {
			ApnsService service = null;
			String payload = null;
			String certFullPath = null;
			Data objData = null;
			File file = null;

			Gson gson = new GsonBuilder().disableHtmlEscaping().create();

			if (deviceIdList != null && deviceIdList.size() > 0) {

				LOG.info("Number of IOS Devices " + deviceIdList.size());

				for (DeviceId device : deviceIdList) {

					objData = getNotificationData(data, device.getHcName(), Constants.PLATFORM_IOS);

					if (null == objData) {
						continue;
					}

					certFullPath = apnsCertPath + device.getHcName() + ".p12";
					file = new File(certFullPath);
					if (!file.exists()) {
						continue;
					}

					try {
						if ("gateway.sandbox.push.apple.com".equals(apnsHost)) {
							service = APNS.newService().withCert(certFullPath, apnsCertPasswd).withSandboxDestination().build();
						} else {
							service = APNS.newService().withCert(certFullPath, apnsCertPasswd).withProductionDestination().build();
						}
					} catch (Exception e) {
						LOG.error("Exception occured :" + e.getMessage());
					}

					iPhoneAlertMsg = objData.getNotiMgs();
					objData.setNotiMgs(null);

					jsonData = gson.toJson(objData);

					LOG.info(jsonData);

					try {

						payload = APNS.newPayload().badge(device.getBadgeCount()).alertBody(jsonData).localizedKey(iPhoneAlertMsg).build();
						payload = payload.replace("\"badge\":" + device.getBadgeCount() + "}}", "\"badge\":" + device.getBadgeCount()
								+ ",\"content-available\":1}}");

						LOG.info("IOS Device ID : " + device.getDeviceId());
						LOG.info("IOS Message Format" + jsonData);
						service.push(device.getDeviceId(), payload);
					} catch (Exception e) {
						LOG.error("Exception occured in sending iOS notification for Token ID: " + device.getDeviceId());
						continue;
					}
				}

				LOG.info("IOS notifications sent successfully");
			} else {
				LOG.info("Devices not found for the IOS Push Notification");
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
			throw new PushNotificationException();
		}
		LOG.info("Method End: sendIOSNotification(), Class: PushNotificationHelper");
		return null;
	}

	/**
	 * Method to send notification for Android.
	 * 
	 * @param deviceId
	 * @param data
	 * @param configList
	 * @return
	 * @throws PushNotificationException
	 * @throws IOException
	 */
	public static String sendAndroidNotification(List<DeviceId> deviceIdList, NotificationDetails data, List<Configuration> configList)
			throws PushNotificationException, IOException {
		LOG.info("Method Start: sendAndroidNotification(), Class: PushNotificationHelper");
		String gcmApiKey = null;
		Data objData = null;

		for (Configuration config : configList) {
			if (Constants.TEXT_GCM_API_KEY.equalsIgnoreCase(config.getConfigurationType())) {
				gcmApiKey = config.getScreenContent();
				break;
			}
		}

		/*
		 * Constructing JSON data.
		 */
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		String gsonData = null;

		boolean isFirstTime = true;

		for (DeviceId id : deviceIdList) {

			// Fetching & Storing HubCiti Information
			if (isFirstTime) {
				objData = getNotificationData(data, id.getHcName(), Constants.PLATFORM_ANDROID);
				if (null == objData) {
					continue;
				}
				gsonData = gson.toJson(objData);
				isFirstTime = false;
			}

			try {
				sendAndroidNotification(gsonData, gcmApiKey, id);
			} catch (PushNotificationException e) {
				LOG.error("Error occured in sending Android notification for Token ID: " + id);
				continue;
			}
		}
		LOG.info("Android notifications sent sussessfully");
		LOG.info("Method End: sendAndroidNotification(), Class: PushNotificationHelper");
		return Constants.SUCCESS_MESSAGE;
	}

	/**
	 * Method to send notification for Android
	 * 
	 * @param jsonData
	 * @param gcmUrl
	 * @param gcmApiKey
	 * @return
	 * @throws PushNotificationException
	 * @throws IOException
	 */

	/**
	 * Method to send notification to android using gcm-server.jar.
	 * 
	 * @param jsonData
	 * @param gcmApiKey
	 * @param regIds
	 * @throws PushNotificationException
	 * @throws IOException
	 */
	public static void sendAndroidNotification(String jsonData, String gcmApiKey, DeviceId regId) throws PushNotificationException, IOException {
		LOG.info("Method Start: sendAndroidNotification(), Class: PushNotificationHelper");
		try {
			Sender sender = new Sender(gcmApiKey);
			Message message = new Message.Builder().addData("data", jsonData).collapseKey(Constants.COLLAPSE_KEY + " for " + regId.getHcName()).build();
			try {
				Result result = sender.sendNoRetry(message, regId.getDeviceId());
				LOG.info("Notification Response : " + result.toString());
				LOG.info("Android Device ID " + regId.getDeviceId());
				LOG.info("Android Message " + jsonData);
			} catch (IOException e) {
				LOG.error(e.getMessage());
			}
		} catch (Exception e) {
			LOG.error("Error occured in sendAndroidNotification :" + e.getMessage());
			throw new PushNotificationException(e.getMessage());
		}
		LOG.info("Method End: sendAndroidNotification(), Class: PushNotificationHelper");
	}

	/**
	 * Method to build Notification Alert text.
	 * 
	 * @param rssMsgCount
	 * @param isDealOfDay
	 * @return
	 */
	public static String getNotificationAlertMessage(Boolean isDealOfDay) {
		LOG.info("Method Start: getNotificationAlertMessage(), Class: PushNotificationHelper");
		String notiMsg = "";

		if (null == isDealOfDay) {
			isDealOfDay = false;
		}
		if (isDealOfDay) {
			notiMsg = "Here is your daily deal.";
		}
		LOG.info("Method End: getNotificationAlertMessage(), Class: PushNotificationHelper");
		return notiMsg;
	}

	/**
	 * Method to get Notification data to sent to device.
	 * 
	 * @param notiData
	 * @param hcName
	 * @param platform
	 * @return
	 */
	public static Data getNotificationData(NotificationDetails notiData, String hcName, String platform) {

		LOG.info("Method Start: getNotificationData(), Class: PushNotificationHelper");

		Boolean dealFlag = false;
		Data data = new Data();

		if (null != notiData.getData().getDealList() && !notiData.getData().getDealList().isEmpty()) {
			List<Deal> dealList = new ArrayList<Deal>();
			for (Deal deal : notiData.getData().getDealList()) {
				Deal objDeal = null;
				if (hcName.equals(deal.getHcName())) {
					dealFlag = true;
					objDeal = new Deal();
					objDeal.setDealId(deal.getDealId());
					if (Constants.PLATFORM_IOS.equalsIgnoreCase(platform)) {
						objDeal.setDealName(deal.getDealName());
					} else {
						objDeal.setDealName(deal.getDealName());
					}
					objDeal.setType(deal.getType());
					objDeal.setSplUrl(deal.getSplUrl());
					objDeal.setRetailerId(deal.getRetailerId());
					objDeal.setRetailLocationId(deal.getRetailLocationId());
					objDeal.setEndDate(deal.getEndDate());
					objDeal.setEndTime(deal.getEndTime());
					//objDeal.setPushDate(deal.getPushDate());
					dealList.add(objDeal);
				}
			}
			if (dealList.size() > 0) {
				data.setDealList(dealList);
			}
		}

		if (dealFlag == true) {
			data.setNotiMgs(getNotificationAlertMessage(dealFlag));
		} else {
			data = null;
		}
		LOG.info("Method End: getNotificationData(), Class: PushNotificationHelper");
		return data;
	}

}
