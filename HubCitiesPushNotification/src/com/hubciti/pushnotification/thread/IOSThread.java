package com.hubciti.pushnotification.thread;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hubciti.pushnotification.common.pojo.Configuration;
import com.hubciti.pushnotification.common.pojo.DeviceId;
import com.hubciti.pushnotification.common.pojo.NotificationDetails;
import com.hubciti.pushnotification.service.PushNotificationHelper;
import com.hubciti.pushnotification.service.PushNotificationService;

/*---------------------------------------------------------------
 * IOSThread for sending push notification to IOS devices
 * 
 * @Author : Kirankumar Garaddi
 * @written : 02/012/2015
 * 
 * Compilation : IOSThread.java
 * Execution   : Java IOSThread
 *     
 * 
 ----------------------------------------------------------------*/
public class IOSThread implements Runnable {

	/**
	 * 
	 * Instance for logging execution information .
	 * 
	 */
	private static final Logger LOG = LoggerFactory.getLogger(PushNotificationService.class);

	/**
	 * Instance for holding ios device information
	 */
	private List<DeviceId> iOSIds;

	/**
	 * Instance for storing notification details
	 */
	private NotificationDetails notiDetails;

	/**
	 * Instance for storing IOS Configuration information
	 */
	private List<Configuration> pushNotiConfigs;

	/**
	 * Thread for running process for fetching separate RSS Feed articles
	 * 
	 */
	private Thread thread;

	/**
	 * 
	 * @return the current thread
	 */
	public Thread getThread() {
		return thread;
	}

	/**
	 * Initialize the instance varibale for holding ios device Ids, Notification
	 * information & configuration information
	 * 
	 * @param iOSIds
	 * @param notiDetails
	 * @param pushNotiConfigs
	 */
	public IOSThread(List<DeviceId> iOSIds, NotificationDetails notiDetails, List<Configuration> pushNotiConfigs) {
		super();
		this.iOSIds = iOSIds;
		this.notiDetails = notiDetails;
		this.pushNotiConfigs = pushNotiConfigs;
		this.thread = new Thread(this);
		this.thread.start();
	}

	/**
	 * 
	 * Send notification to IOS (APNS Server)
	 * 
	 */
	@Override
	public void run() {

		try {

			PushNotificationHelper.sendIOSNotification(iOSIds, notiDetails, pushNotiConfigs);

		} catch (IOException e) {

			LOG.error("Error Occured in sending iOS notification: " + e.getMessage());

		} catch (Exception e) {

			LOG.error("Error Occured in sending iOS notification: " + e.getMessage());

		}

	}

}
