package com.hubciti.pushnotification.dao;

import java.util.List;

import com.hubciti.pushnotification.common.pojo.Configuration;
import com.hubciti.pushnotification.common.pojo.Data;
import com.hubciti.pushnotification.exception.PushNotificationException;

public interface PushNotificationDao {


	/**
	 * DAO method to get notification configuration.
	 * @return
	 * @throws PushNotificationException
	 */
	public List<Configuration> getPushNotificationConfiguration(Integer hubCitiId) throws PushNotificationException;
	
	/**
	 * DAO method to get notification data.
	 * @return
	 * @throws PushNotificationException
	 */
	public Data getNotificationMessages(Integer hubCitiId) throws PushNotificationException;
}
